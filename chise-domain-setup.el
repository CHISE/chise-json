;; -*- coding: utf-8-mcs-er -*-
(setq file-name-coding-system 'utf-8-mcs-er)

(concord-object-put (concord-make-object 'domain 'shuowen)
		    'name@zh "說文解字")
(concord-object-put (concord-make-object 'domain 'shuowen)
		    'name@ja "説文解字")
