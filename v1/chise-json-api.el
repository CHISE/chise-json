;; -*- coding: utf-8-mcs-er -*-
(require 'cwiki-format)
(require 'json)
(require 'ids-find)
(require 'ja-romaji-to-kana)

(defvar chise-json-dump-directory "/usr/local/share/chise-dump-json/")

(setq file-name-coding-system 'utf-8-mcs-er)

(make-coding-system
 'utf-16-le-mcs-no-composition 'utf-16
 "Coding-system of UTF-16be without composition."
 '(mnemonic "MTF16le-nc" disable-composition t))

(mount-char-attribute-table 'ideographic-products)


(unless (fboundp 'orig:json-encode)
  (fset 'orig:json-encode (symbol-function #'json-encode)))

(defun chise-json-encode-object* (object)
  (let (ucs ret)
    (cond
     ((characterp object)
      (if (setq ucs (encode-char object '=ucs))
	  (if (<= ucs #xFFFF)
	      (char-to-string object)
	    (setq ret (encode-coding-string (char-to-string object)
					    'utf-16-le-mcs-no-composition))
	    (format "\\u%2X%02X\\u%02X%02X"
		    (aref ret 1)
		    (aref ret 0)
		    (aref ret 3)
		    (aref ret 2)
		    ))
	(cons '(@type . concord:genre/character)
	      (chise-json-char-get-ref-info object)))
      )
     ((concord-object-p object)
      (list (cons '@type (format "concord:genre/%s" (concord-object-genre object)))
	    (cons '@id (chise-rdf-iri-encode-object object)))
      ))))

(defun chise-json-encode-string (string)
  (let (ucs ret)
    (mapconcat (lambda (object)
		 (if (setq ucs (encode-char object '=ucs))
		     (if (<= ucs #xFFFF)
			 (char-to-string object)
		       (setq ret (encode-coding-string (char-to-string object)
						       'utf-16-le-mcs-no-composition))
		       (format "\\u%2X%02X\\u%02X%02X"
			       (aref ret 1)
			       (aref ret 0)
			       (aref ret 3)
			       (aref ret 2)
			       ))
		   (encode-coding-string (char-to-string object)
					 'utf-8-mcs-er)))
	       string "")))

(defun chise-json-encode-ucs-string (string)
  (let (ucs ret)
    (mapconcat (lambda (object)
		 (if (or (setq ucs (encode-char object '=ucs))
			 (if (and (setq ucs (char-ucs object))
				  (setq ret (decode-char '=ucs ucs)))
			     (setq object ret)))
		     (if (<= ucs #xFFFF)
			 (char-to-string object)
		       (setq ret (encode-coding-string (char-to-string object)
						       'utf-16-le-mcs-no-composition))
		       (format "\\u%2X%02X\\u%02X%02X"
			       (aref ret 1)
			       (aref ret 0)
			       (aref ret 3)
			       (aref ret 2)
			       ))
		   (encode-coding-string (char-to-string object)
					 'utf-8-mcs-er)))
	       string "")))

(defun json-encode (object)
  "Return a JSON representation of OBJECT as a string."
  (if (or (characterp object)
	  (concord-object-p object))
      (setq object (chise-json-encode-object* object)))
  (orig:json-encode object))

(defun chise-json-encode-object (object)
  (encode-coding-string (json-encode object) 'utf-8-mcs-er))


(defun chise-rdf-iri-make-ccs@domain (base context)
  (let ((base-iri (char-feature-property base '=json-ld-item)))
    (unless base-iri
      (setq base-iri (www-uri-encode-feature-name base)))
    (if context
	(intern (format "%s@%s" base-iri context))
      base-iri)))

(defun chise-rdf-iri-encode-ccs-spec (base granularity context)
  (format "%s:%s"
	  granularity
	  (chise-rdf-iri-make-ccs@domain base context)))

(defun chise-rdf-iri-encode-ccs-name (feature-name)
  (apply #'chise-rdf-iri-encode-ccs-spec (chise-split-ccs-name feature-name)))

(defun chise-rdf-iri-decode-ccs-name (iri-feature)
  (let (iri-base base base-cobj iri-granularity ccs-prefix
		 iri-domain domain)
    (if (string-match "@" iri-feature)
	(setq iri-base (substring iri-feature 0 (match-beginning 0))
	      iri-domain (substring iri-feature (match-end 0)))
      (setq iri-base iri-feature))
    (if (string-match ":" iri-base)
	(setq iri-granularity (substring iri-base 0 (match-beginning 0))
	      iri-base (substring iri-base (match-end 0))))
    (when iri-granularity
      (setq base
	    (if (equal iri-base "ucs")
		'ucs
	      (or (and (setq base-cobj
			     (concord-decode-object
			      '=json-ld-item (intern iri-base) 'feature))
		       (concord-object-id base-cobj))
		  (est-uri-decode-feature-name iri-base))))
      (setq ccs-prefix (chise-encode-ccs-prefix (intern iri-granularity)))
      (setq domain
	    (if iri-domain
		(est-uri-decode-feature-name-body iri-domain)))
      (if domain
	  (intern (format "%s%s@%s" ccs-prefix base domain))
	(intern (format "%s%s" ccs-prefix base))))))

(defun chise-rdf-iri-encode-feature-name (feature-name)
  (or (char-feature-property feature-name '=json-ld-item)
      (if (find-charset feature-name)
	  (chise-rdf-iri-encode-ccs-name feature-name)
	(www-uri-encode-feature-name feature-name))))

(defun chise-rdf-iri-decode-feature-name (iri-feature-name)
  (let ((feature-cobj (concord-decode-object
		       '=json-ld-item (intern iri-feature-name) 'feature)))
    (or (if feature-cobj
	    (concord-object-id feature-cobj))
	(chise-rdf-iri-decode-ccs-name iri-feature-name)
	(est-uri-decode-feature-name iri-feature-name))))

(defun chise-rdf-iri-encode-object (object)
  (if (characterp object)
      (if (encode-char object '=ucs)
	  (concat
	   "character:"
	   (mapconcat
	    (lambda (byte)
	      (format "%%%02X" byte))
	    (encode-coding-string (char-to-string object) 'utf-8-mcs-er)
	    ""))
	(let ((ccs-list est-coded-charset-priority-list)
	      ccs ret)
	  (while (and ccs-list
		      (setq ccs (pop ccs-list))
		      (not (setq ret (encode-char object ccs 'defined-only)))))
	  (cond (ret
		 (format "%s/%s"
			 (chise-rdf-iri-encode-feature-name ccs)
			 (chise-ccs-format-code-point ccs ret "0x"))
		 )
		((and (setq ccs (car (split-char object)))
		      (setq ret (encode-char object ccs)))
		 (format "%s/%s"
			 (chise-rdf-iri-encode-feature-name ccs)
			 (chise-ccs-format-code-point ccs ret "0x"))
		 )
		(t
		 (format "system-char-id:0x%X"
			 (encode-char object 'system-char-id))
		 ))))
    (format "concord:%s/%s"
	    (chise-rdf-iri-encode-feature-name (concord-object-genre object))
	    (chise-rdf-iri-encode-feature-name (concord-object-id object)))))

(defun chise-rdf-iri-decode-object (object-rep)
  (let (prefix body ccs-prefix genre id base ccs)
    (cond
     ((string-match ":" object-rep)
      (setq prefix (substring object-rep 0 (match-beginning 0))
	    body (substring object-rep (match-end 0)))
      (cond
       ((equal prefix "concord")
	(when (string-match "/" body)
	  (setq genre (chise-rdf-iri-decode-feature-name
		       (substring body 0 (match-beginning 0)))
		id (chise-rdf-iri-decode-feature-name
		    (substring body (match-end 0))))
	  (concord-decode-object '=id id genre)
	  )
	)
       ((setq ccs-prefix (chise-encode-ccs-prefix (intern prefix)))
	(when (string-match "/" body)
	  (setq base (chise-rdf-iri-decode-feature-name
		      (substring body 0 (match-beginning 0)))
		id (substring body (match-end 0)))
	  (when (find-charset (setq ccs (intern (format "%s%s" ccs-prefix base))))
	    (cond
	     ((string-match "^0x" id)
	      (setq id (string-to-number (substring id (match-end 0)) 16))
	      )
	     (t
	      (setq id (car (read-from-string id)))
	      ))
	    (decode-char ccs id)
	    ))
	))
      ))
    ))

(defun chise-feature-name-split-metadata (feature-name)
  (let (ret)
    (setq feature-name (symbol-name feature-name))
    (if (string-match ".\\(\\$_\\([0-9]+\\)\\)?\\*." feature-name)
	(list (intern (substring feature-name 0 (1+ (match-beginning 0))))
	      (if (setq ret (match-string 2 feature-name))
		  (car (read-from-string ret)))
	      (intern (substring feature-name (1- (match-end 0))))))))

(defun chise-feature-name-split-domain (feature-name)
  (setq feature-name (symbol-name feature-name))
  (if (string-match "@[^@/*]+\\(/[^@/*]+\\)*$" feature-name)
      (cons (intern (substring feature-name 0 (match-beginning 0)))
	    (intern (substring feature-name (1+ (match-beginning 0)))))))


(defun chise-split-ccs-name (ccs)
  (cond ((eq ccs '=ucs)
	 '(ucs abstract-character nil)
	 )
	((eq ccs '=big5)
	 '(big5 abstract-character nil)
	 )
	(t
	 (let ((ccs-name (symbol-name
			  (let ((ccs-obj (find-charset ccs)))
			    (if ccs-obj
				(charset-name ccs-obj)
			      ccs))))
	       ret)
	   (if (string-match "^\\(=[=+>]*\\)\\([^=>@*]+\\)@?" ccs-name)
	       (list (intern (match-string 2 ccs-name))
		     (chise-decode-ccs-prefix (match-string 1 ccs-name))
		     (if (string= (setq ret (substring ccs-name (match-end 0))) "")
			 nil
		       (car (read-from-string ret))))
	     (list ccs 'abstract-character nil)))
	 )))

(defconst chise-ccs-prefix-alist
  '(("==>" . super-abstract-character)
    ("==>" . a2)
    ("=>"  . abstract-character)
    ("=>"  . character)
    ("=>"  . a)
    ("=+>" . unified-glyph)
    ("=+>" . o)
    ("="   . abstract-glyph)
    ("="   . glyph)
    ("="   . rep)
    ("=>>" . detailed-glyph)
    ("=>>" . g)
    ("=="  . abstract-glyph-form)
    ("=="  . glyph-form)
    ("=="  . g2)
    ("===" . glyph-image)
    ("===" . repi)
    ))

(defun chise-split-id-feature-name (id-feature)
  (let ((chise-ccs-prefix-alist
	 (cons '("=" . rep)
	       chise-ccs-prefix-alist)))
    (chise-split-ccs-name id-feature)))

(defun chise-decode-ccs-prefix (ccs-prefix)
  (or (cdr (assoc ccs-prefix chise-ccs-prefix-alist))
      'character))

(defun chise-encode-ccs-prefix (ccs-granularity)
  (car (rassoc ccs-granularity chise-ccs-prefix-alist)))

(defun chise-ccs-format-code-point (ccs value &optional hex-prefix rep-ccs)
  (let ((presentation-type
	 (char-feature-property ccs 'value-presentation-type))
	(presentation-minimum-width
	 (char-feature-property ccs 'value-presentation-minimum-width))
	ret ccs-base)
    (unless rep-ccs
      (cond ((find-charset ccs)
	     (setq rep-ccs ccs)
	     )
	    ((and (setq rep-ccs (intern (format "=%s" ccs)))
		  (find-charset rep-ccs))
	     )
	    ((and (setq rep-ccs (intern (format "=>%s" ccs)))
		  (find-charset rep-ccs))
	     )
	    ((and (setq rep-ccs (intern (format "==>%s" ccs)))
		  (find-charset rep-ccs))
	     )
	    ((and (setq rep-ccs (intern (format "===%s" ccs)))
		  (find-charset rep-ccs))
	     )))
    (unless hex-prefix
      (setq hex-prefix "#x"))
    (unless (and presentation-type presentation-minimum-width)
      (setq ret (chise-split-ccs-name ccs))
      (setq ccs-base (car ret))
      (unless presentation-type
	(setq presentation-type
	      (char-feature-property ccs-base 'value-presentation-type)))
      (unless presentation-minimum-width
	(setq presentation-minimum-width
	      (char-feature-property ccs-base 'value-presentation-minimum-width))))
    (format
     (cond
      ((eq presentation-type 'decimal)
       (if presentation-minimum-width
	   (format "%%0%dd" presentation-minimum-width)
	 "%d")
       )
      ((eq presentation-type 'HEX)
       (if presentation-minimum-width
	   (format "%s%%0%dX" hex-prefix presentation-minimum-width)
	 (format "%s%%X" hex-prefix))
       )
      ((eq presentation-type 'hex)
       (if presentation-minimum-width
	   (format "%s%%0%dx" hex-prefix presentation-minimum-width)
	 (format "%s%%x" hex-prefix))
       )
      (t
       (if (>= (charset-dimension rep-ccs) 2)
	   (format "%s%%04X" hex-prefix)
	 (format "%s%%02X" hex-prefix)
	 )
       ))
     (if (= (charset-iso-graphic-plane rep-ccs) 1)
	 (logior value
		 (cond ((= (charset-dimension rep-ccs) 1)
			#x80)
		       ((= (charset-dimension rep-ccs) 2)
			#x8080)
		       ((= (charset-dimension rep-ccs) 3)
			#x808080)
		       (t 0)))
       value))))

      
(defun chise-json-encode-feature-name (feature-name)
  (or (char-feature-property feature-name '=json-ld-item)
      (if (find-charset feature-name)
	  (chise-turtle-uri-encode-ccs-name feature-name)
	(www-uri-encode-feature-name feature-name))))

(defun chise-json-make-value-cell (value)
  (cons 'body
	(if (atom value)
	    value
	  (mapvector (lambda (obj)
		       (if (and (consp obj)
				(consp (car obj))
				(and (symbolp (caar obj))))
			   (concord-object-spec-to-full-nested-form obj)
			 obj))
		     value))))

(defun concord-object-spec-to-full-nested-form (spec-alist)
  (setq spec-alist (copy-tree spec-alist))
  (let (nested-spec
        feat val
        target target-base target-domain target-index
        meta meta-base meta-domain
        ret ret2 ret3 ret4 ret5)
    (dolist (cell (sort spec-alist
                        (lambda (a b)
                          (char-attribute-name< (car a)(car b)))))
      (setq feat (car cell)
            val (cdr cell))
      (if (eq feat 'composition)
	  (setq nested-spec
		(cons (list (car cell)
			    (list nil (cons 'body (vector (cdr cell)))))
		      nested-spec))
	(if (setq ret (chise-feature-name-split-metadata feat))
	    (setq target (car ret)
		  target-index (nth 1 ret)
		  meta (nth 2 ret))
	  (setq target feat
		target-index nil
		meta nil))
	(if (setq ret (chise-feature-name-split-domain target))
	    (setq target-base (car ret)
		  target-domain (cdr ret))
	  (setq target-base target
		target-domain nil))
	(if (setq ret (chise-feature-name-split-domain meta))
	    (setq meta-base (car ret)
		  meta-domain (cdr ret))
	  (setq meta-base meta
		meta-domain nil))
	(if (setq ret (assq target-base nested-spec))
	    (cond
	     ((setq ret2 (assq target-domain (cdr ret)))
	      (cond
	       (target-index
		(setq ret3 (assq 'body (cdr ret2)))
		(setq ret3 (cdr ret3))
		(setq ret4 (aref ret3 (1- target-index)))
		(if (and (consp ret4)
			 (assq 'body ret4))
		    (if (setq ret5 (assq meta-base ret4))
			(setcdr ret5 (cons (cons meta-domain
						 (list (chise-json-make-value-cell
							val)))
					   (cdr ret5)))
		      (aset ret3 (1- target-index)
			    (cons (cons meta-base
					(list (cons meta-domain
						    (list
						     (chise-json-make-value-cell
						      val)))))
				  ret4)))
		  (aset ret3 (1- target-index)
			(list (cons 'body ret4)
			      (cons meta-base
				    (list (cons meta-domain
						(list
						 (chise-json-make-value-cell
						  val))))))))
		)
	       (meta-base
		(cond
		 ((setq ret5 (assq meta-base (cdr ret2)))
		  (setcdr ret5 (cons (cons meta-domain
					   (chise-json-make-value-cell val))
				     (cdr ret5)))
		  )
		 (t
		  (setcdr ret2 (cons (cons meta-base
					   (list (cons meta-domain
						       (list
							(chise-json-make-value-cell
							 val)))))
				     (cdr ret2)))
		  ))
		))
	      )
	     (t
	      (setcdr ret (cons (cons target-domain
				      (list (chise-json-make-value-cell val)))
				(cdr ret)))
	      ))
	  (setq nested-spec
		(cons (cons target-base
			    (list (cons target-domain
					(list (chise-json-make-value-cell val)))))
		      nested-spec))
	  )))
    nested-spec))

(defun concord-nested-feature-domain-spec-simplify (dom-spec)
  (let (spec cell)
    (cond
     ((null (car dom-spec))
      (setq spec (cdr dom-spec))
      (if (null (cdr spec))
	  (if (and (setq cell (car spec))
		   (eq (car cell) 'body))
	      (cond
	       ((vectorp (cdr cell))
		(if (= (length (cdr cell)) 1)
		    (concord-full-nested-feature-simplify (aref (cdr cell) 0))
		  (mapvector #'concord-full-nested-feature-simplify (cdr cell)))
		)
	       ((atom (cdr cell))
		(cdr cell)
		)
	       ((atom (cadr cell))
		(mapvector #'identity (cdr cell))
		)
	       (t
		(concord-full-nested-feature-simplify spec)))
	    (concord-full-nested-feature-simplify spec))
	(concord-full-nested-feature-simplify spec))
      )
     ((eq (car dom-spec) 'body)
      dom-spec)
     (t
      (cons (cons 'context (format "domain:%s" (car dom-spec)))
	    (concord-full-nested-feature-simplify (cdr dom-spec)))
      ))
    ))

(defun concord-full-nested-feature-simplify (full-nested-feature)
  (if (atom full-nested-feature)
      full-nested-feature
    (mapcar (lambda (cell)
	      (if (eq (car cell) 'body)
		  (cond ((vectorp (cdr cell))
			 (cons (car cell)
			       (mapvector #'concord-full-nested-feature-simplify
					  (cdr cell)))
			 )
			((atom (cdr cell))
			 cell
			 )
			((atom (cadr cell))
			 (cons (car cell)
			       (mapvector #'identity cell))
			 )
			(t
			 (cons (car cell)
			       (mapvector #'concord-full-nested-feature-simplify
					  (cdr cell)))))
                (cons (car cell)
		      ;; (if (symbolp (car cell))
                      ;;     (chise-json-encode-feature-name (car cell))
                      ;;   (car cell))
		      (cond
		       ((atom (cdr cell))
			(cdr cell)
			)
		       ((null (cddr cell))
			(concord-nested-feature-domain-spec-simplify
			 (cadr cell))
			)
		       (t
			(mapvector #'concord-nested-feature-domain-spec-simplify
				   (cdr cell)))))))
	    full-nested-feature)))

;; (defun concord-object-spec-to-nested-form (spec-alist)
;;   (concord-full-nested-feature-simplify
;;    (concord-object-spec-to-full-nested-form
;;     spec-alist)))

(defun chise-json-encode-general-feature-pair (feature-pair)
  (let ((feature-name (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	item context context-cell
	new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq item (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context-cell cell
			 context (cdr cell))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq item body))
      (if context
	  (setq new-body (cons context-cell new-body)))
      (setq dest (cons (if new-body
			   (if item
			       (cons (cons 'rdf:value item)
				     new-body)
			     new-body)
			 item)
		       dest))
      (setq i (1+ i)
	    new-body nil))
    (cons (chise-json-encode-feature-name feature-name)
	  (if (cdr dest)
	      (mapvector #'identity (nreverse dest))
	    (car dest)))))

(defun chise-json-encode-name-feature-pair (feature-pair)
  (let ((feature-name (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	item context
	lang
	new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq item (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context (cdr cell))
		   (setq lang
			 (if (string-match "^domain:" context)
			     (substring context (match-end 0))
			   context))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq item body))
      (setq dest (cons (cons (or lang "@none") item)
		       dest))
      (setq i (1+ i)
	    new-body nil))
    (cons (chise-json-encode-feature-name feature-name)
	  (if (or (cdr dest)
		  (not (string= (caar dest) "@none")))
	      dest
	    (cdar dest)))))

(defun chise-json-encode-ccs-feature-pair (feature-pair)
  (let ((ccs (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	code-point code-point-rep context context-cell
	ret base granularity
	bare-ccs@domain ccs@domain new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  ;; (setq code-point (cdr (assq 'body body))
	  ;;       context (cdr (assq 'context body)))
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq code-point (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context-cell cell
			 context (cdr cell))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq code-point body))
      (if (eq ccs '=ucs)
	  (if context
	      (setq base 'ucs
		    granularity 'abstract-glyph)
	    (setq base 'ucs
		  granularity 'abstract-character))
	(setq ret (chise-split-ccs-name ccs)
	      base (pop ret)
	      granularity (pop ret)))
      (setq code-point-rep
	    (chise-ccs-format-code-point
	     base code-point "0x"))
      (setq bare-ccs@domain
	    (if context
		(intern (format "%s@%s"
				base (substring context 7)))
	      base))
      (setq ccs@domain (intern (format "%s:%s" granularity bare-ccs@domain)))
      (if context
	  (setq new-body (cons context-cell new-body)))
      (setq dest
	    (cons
	     (cons ccs@domain
		   (list*
		    (cons '@id (format "%s/%s"
				       ccs@domain
				       code-point-rep))
		    (cons (intern (format "%s-of" granularity))
			  (list (cons '@id (format "bare-ccs:%s/%s"
						   base code-point-rep))
				(cons 'CCS (format "ccs:%s" base))
				(cons 'code-point code-point)))
		    new-body))
	     dest))
      (setq i (1+ i)
	    new-body nil))
    (nreverse dest)))

(defun chise-json-encode-id-feature-pair (feature-pair genre)
  (let ((id-feature (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	value-rep context context-cell
	ret base granularity
	bare-id-feature@domain id-feature@domain new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq value (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context-cell cell
			 context (cdr cell))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq value body))
      (setq ret (chise-split-id-feature-name id-feature)
	    base (pop ret)
	    granularity (pop ret))
      (setq value-rep (format "%s" value))
      (setq bare-id-feature@domain
	    (if context
		(intern (format "%s@%s"
				base (substring context 7)))
	      base))
      (setq id-feature@domain
	    (intern (format "%s:%s" granularity bare-id-feature@domain)))
      (if context
	  (setq new-body (cons context-cell new-body)))
      (setq dest
	    (cons
	     (cons id-feature@domain
		   (list*
		    (cons '@id (format "concord-idx:%s/%s/%s"
				       genre
				       id-feature@domain
				       value-rep))
		    (cons 'identifier
			  (list '(@type . "PropertyValue")
				(cons 'propertyID
				      (format "rep:%s" base))
				(cons 'value value)))
		    new-body))
	     dest))
      (setq i (1+ i)
	    new-body nil))
    (nreverse dest)))

(defun chise-json-encode-ccs-spec (ccs-spec)
  (cons 'unify
	(mapcan #'chise-json-encode-ccs-feature-pair ccs-spec)))

(defun chise-json-encode-id-spec (id-spec genre)
  (if (eq genre 'character)
      (chise-json-encode-ccs-spec id-spec)
    (cons 'unify
	  (mapcan (lambda (cell)
		    (chise-json-encode-id-feature-pair cell genre))
		  id-spec))))

(defun chise-json-encode-variant-body (val place-id json-feature-name)
  (let (body context source new-body)
    (if (atom val)
	(setq body val)
      (dolist (cell val)
	(cond
	 ((eq (car cell) 'body)
	  (setq body (cdr cell))
	  )
	 ((eq (car cell) 'context)
	  (setq context (cdr cell))
	  )
	 ((eq (car cell) 'sources)
	  (setq source (cdr cell))
	  )
	 (t
	  (setq new-body (cons cell new-body))
	  ))))
    (if source
	(setq new-body
	      (cons (cons 'source
			  (if (vectorp source)
			      (mapvector (lambda (item)
					   (format "domain:%s" item))
					 source)
			    (format "domain:%s" source)))
		    new-body)))
    (if context
	(setq new-body
	      (cons (cons 'context context)
		    new-body)))
    (list* (cons '@id place-id)
	   (list 'ideolink
		 (cons '@id (format "%s/link" place-id))
		 (cons json-feature-name
		       (if (vectorp body)
			   (mapvector #'chise-rdf-iri-encode-object body)
			 (chise-rdf-iri-encode-object body))))
	   new-body)))

(defun chise-json-encode-variant-feature-pair (feature-pair char-id)
  (let ((feature-name (car feature-pair))
	(val (cdr feature-pair))
	body context context-name source new-body
	json-feature-name i
	new-body-content
	node-id)
    (if (atom val)
	(setq body val)
      (dolist (cell val)
	(cond
	 ((eq (car cell) 'body)
	  (setq body (cdr cell))
	  )
	 ((eq (car cell) 'context)
	  (setq context (cdr cell))
	  )
	 ((eq (car cell) 'sources)
	  (setq source (cdr cell))
	  )
	 (t
	  (setq new-body (cons cell new-body))
	  ))))
    (setq json-feature-name
	  (chise-json-encode-feature-name feature-name))
    (if source
	(setq new-body
	      (cons (cons 'source
			  (if (vectorp source)
			      (mapvector (lambda (item)
					   (format "domain:%s" item))
					 source)
			    (format "domain:%s" source)))
		    new-body)))
    (if context
	(setq new-body
	      (cons (cons 'context context)
		    new-body)))
    (setq node-id
	  (format "%s/%s%s"
		  char-id json-feature-name
		  (if context
		      (progn
			(if (symbolp context)
			    (setq context-name (symbol-name context))
			  (setq context-name context))
			(if (string-match "^domain:" context-name)
			    (setq context-name
				  (substring context-name (match-end 0))))
			(format "@%s" context-name))
		    "")))
    (setq new-body-content
	  (cond
	   ((vectorp body)
	    (setq i 0)
	    (mapvector (lambda (item)
			 (setq i (1+ i))
			 (chise-json-encode-variant-body
			  item
			  (format "%s$_%d" node-id i)
			  json-feature-name))
		       body)
	    )
	   (t
	    (chise-json-encode-variant-body
	     body
	     (format "%s/%s" char-id json-feature-name)
	     json-feature-name)
	    )))
    (cons json-feature-name
	  (if new-body
	      (list* (cons '@id node-id)
		     (cons 'body new-body-content)
		     new-body)
	    new-body-content))))

(defun chise-json-encode-variant-spec (variant-spec char-id
						    &optional spec-category)
  (cons (or spec-category 'variant)
	(mapcar (lambda (feature-pair)
		  (chise-json-encode-variant-feature-pair feature-pair char-id))
		variant-spec)))

(defun chise-json-api-character-decode (args)
  (let (ccs cpos char format-type ret)
    (when (and (setq ccs (assoc "ccs" args))
	       (setq ccs (cdr ccs))
	       (setq cpos (assoc "code-point" args))
	       (setq cpos (cdr cpos))
	       (setq ccs (chise-rdf-iri-decode-feature-name ccs))
	       (find-charset ccs))
      (cond
       ((string-match "^0x" cpos)
	(setq cpos (string-to-number (substring cpos (match-end 0)) 16))
	)
       (t
	(setq cpos (car (read-from-string cpos)))
	))
      (if (setq format-type (assoc "format" args))
	  (setq format-type (cdr format-type)))
      (when (and (integerp cpos)
		 (setq char (decode-char ccs cpos)))
	(cond
	 ((and format-type
	       (or (equal (downcase format-type) "est")
		   (equal (downcase format-type) "string")))
	  (encode-coding-string
	   (json-encode
	    (www-uri-encode-object char))
	   'utf-8-mcs-er)
	  )
	 ((equal format-type "json")
	  (chise-json-encode-object char)
	  )
	 (t
	  (setq ret (chise-json-encode-object* char))
	  (cond
	   ((stringp ret)
	    (cons
	     (encode-coding-string
	      (json-encode
	       (list '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     '(@type . genre:character)
		     (cons '@id (format "character:%s" ret))))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    )
	   (t
	    (cons
	     (encode-coding-string
	      (json-encode
	       (cons '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     ret))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    ))))))))

(defun chise-json-api-character-encode (args)
  (let (ccs char cpos)
    (when (and (setq char (assoc "character" args))
	       (setq char (cdr char))
	       (setq char (or (chise-rdf-iri-decode-object char)
			      (www-uri-decode-object 'character char)))
	       (setq ccs (assoc "ccs" args))
	       (setq ccs (cdr ccs))
	       (setq ccs (chise-rdf-iri-decode-feature-name ccs))
	       (find-charset ccs))
      (if (setq cpos (encode-char char ccs))
	  (format "%d" cpos)))))

(defun chise-json-api-character-get (args)
  (let (char feat val)
    (when (and (setq char (assoc "character" args))
	       (setq char (cdr char))
	       (setq char (or (chise-rdf-iri-decode-object char)
			      (www-uri-decode-object 'character char)))
	       (setq feat (assoc "feature" args))
	       (setq feat (cdr feat))
	       (setq feat (chise-rdf-iri-decode-feature-name feat)))
      (if (setq val (char-feature char feat))
	  (encode-coding-string (json-encode val) 'utf-8-mcs-er)))))

;; (defun chise-json-api-character-get-spec (args)
;;   (let ((media-type "application/json")
;;         char ret feature-format)
;;     (when (and (setq char (assoc "character" args))
;;                (setq char (cdr char))
;;                (setq char (or (chise-rdf-iri-decode-object char)
;;                               (www-uri-decode-object 'character char))))
;;       (when (setq ret (del-alist '*instance@ruimoku/bibliography/title
;;                                  (del-alist '*instance@morpheme-entry/zh-classical
;;                                             (del-alist 'ideographic-products
;;                                                        (char-attribute-alist char)))))
;;         (if (setq feature-format (assoc "feature-format" args))
;;             (setq feature-format (cdr feature-format)))
;;         (cons (encode-coding-string
;;                (json-encode
;;                 (cond
;;                  ((equal feature-format "native")
;;                   (sort ret
;;                         (lambda (a b)
;;                           (string< (car a)(car b))))
;;                   )
;;                  ((equal feature-format "json")
;;                   (mapcar (lambda (pair)
;;                             (cons (www-uri-encode-feature-name (car pair))
;;                                   (cdr pair)))
;;                           (sort ret
;;                                 (lambda (a b)
;;                                   (string< (car a)(car b)))))
;;                   )
;;                  (t ; (equal feature-format "json-ld")
;;                   (setq media-type "application/ld+json")
;;                   (concord-object-get-json-ld-spec char ret)
;;                   )))
;;                'utf-8-mcs-er)
;;               media-type)))))

(defun chise-json-api-character-ids-match (args)
  (let ((media-type "application/json")
	structure ret)
    (when (and (setq structure (assoc "ids" args))
	       (setq structure (cdr structure))
	       (setq structure (ids-parse-string
				(decode-uri-string structure 'utf-8-mcs-er)))
	       (setq structure (cdr (assq 'ideographic-structure structure)))
	       (setq ret (ideographic-structure-find-chars structure)))
      (cons (encode-coding-string (json-encode ret) 'utf-8-mcs-er)
	    media-type))))


(defun chise-json-char-get-radical-strokes (char)
  (let ((radical (get-char-attribute char 'ideographic-radical))
	(strokes (get-char-attribute char 'ideographic-strokes))
	spec dest rad-cell rad-name-cell str-cell domain)
    (if radical
	(progn
	  (setq rad-cell (cons 'kangxi-radical-number radical)
		rad-name-cell (cons 'kangxi-radical
				    (char-to-string (ideographic-radical radical))))
	  (if strokes
	      (list
	       (list rad-cell
		     rad-name-cell
		     (cons 'kangxi-strokes strokes)))
	    (setq spec (char-attribute-alist char))
	    (dolist (cell spec)
	      (when (string-match "^ideographic-strokes@\\([^*]+\\)$"
				  (symbol-name (car cell)))
		(setq domain (intern (match-string 1 (symbol-name (car cell)))))
		(setq dest
		      (cons (list rad-cell
				  rad-name-cell
				  (cons 'kangxi-strokes (cdr cell))
				  (cons 'domain domain))
			    dest))))
	    dest))
      (if strokes
	  (progn
	    (setq str-cell (cons 'kangxi-strokes strokes))
	    (setq spec (char-attribute-alist char))
	    (dolist (cell spec)
	      (when (string-match "^ideographic-radical@\\([^*]+\\)$"
				  (symbol-name (car cell)))
		(setq domain (intern (match-string 1 (symbol-name (car cell)))))
		(setq dest
		      (cons (list (cons 'kangxi-radical-number (cdr cell))
				  (cons 'kangxi-radical
					(char-to-string (ideographic-radical (cdr cell))))
				  str-cell
				  (cons 'domain domain))
			    dest))))
	    dest)
	(setq spec (char-attribute-alist char))
	(dolist (cell spec)
	  (when (string-match "^ideographic-radical@\\([^*]+\\)$"
			      (symbol-name (car cell)))
	    (setq domain (intern (match-string 1 (symbol-name (car cell)))))
	    (setq strokes (get-char-attribute
			   char
			   (intern (format "ideographic-strokes@%s" domain))))
	    (setq dest
		  (cons (list* (cons 'kangxi-radical-number (cdr cell))
			       (cons 'kangxi-radical
				     (char-to-string (ideographic-radical (cdr cell))))
			       (if strokes
				   (list (cons 'kangxi-strokes strokes)
					 (cons 'domain domain))
				 (cons 'domain domain)))
			dest))))
	dest))))

(defun chise-json-char-get-root (char)
  (let ((mother (or (get-char-attribute char '<-subsumptive)
		    (get-char-attribute char '<-denotational))))
    (if mother
	(chise-json-char-get-root (if (consp mother)
				      (car mother)
				    mother))
      char)))

(defun chise-json-char-get-normalized-spec (char &optional subnode spec-alist)
  (unless spec-alist
    (setq spec-alist (char-attribute-alist char)))
  (let (dest
	metadata-features
	md-spec
	fcell item-num body-vec)
    (dolist (cell spec-alist)
      (when (if (or (and subnode
			 (memq (car cell) '(<-denotational
					    <-subsumptive)))
		    (memq (car cell) '(name
				       general-category bidi-category mirrored
				       hanyu-dazidian
				       *instance@ruimoku/bibliography/title
				       *instance@morpheme-entry/zh-classical
				       ->HDIC-KTB ->HDIC-SYP ->HDIC-TSJ
				       =hdic-syp-entry-id
				       =hdic-ktb-entry-id
				       hdic-syp-description
				       hdic-ktb-description
				       hdic-ktb-entry-type
				       hdic-ktb-ndl-pid
				       ->HNG@CN/manuscript
				       <-HNG@CN/manuscript
				       ->HNG@CN/printed
				       <-HNG@CN/printed
				       ->HNG@JP/manuscript
				       <-HNG@JP/manuscript
				       ->HNG@JP/printed
				       <-HNG@JP/printed
				       ->HNG@KR
				       <-HNG@KR
				       ->HNG@MISC
				       <-HNG@MISC
                                       ;; =decomposition@cid
                                       ;; =decomposition@hanyo-denshi
                                       ;; =decomposition@mj
				       abstract-glyph@iwds-1)))
		nil
	      (if (and (memq (car cell) '(<-denotational
					  <-subsumptive))
		       (characterp (cdr cell)))
		  (setcdr cell (list (cdr cell))))
	      t)
	(cond
	 ((setq md-spec (chise-feature-name-split-metadata (car cell)))
	  (setq metadata-features (cons (cons (if (consp (cdr cell))
						  (apply #'vector (cdr cell))
						(vector (cdr cell)))
					      md-spec)
					metadata-features))
	  )
	 ((and (find-charset (car cell))
	       (null (cdr cell)))
	  )
	 ((atom (cdr cell))
	  (setq dest
		(cons (cons (car cell)
			    (list (cons 'body (cdr cell))))
		      dest))
	  )
	 ((or (eq 'hanyu-dazidian (car cell))
	      (string-match "^\\(ideographic-structure\\|=decomposition\\)\\($\\|@\\)"
			    (symbol-name (car cell))))
	  (setq dest
		(cons (cons (car cell)
			    (list (cons 'body (apply #'vector (cdr cell)))))
		      dest))
	  )
	 (t
	  (setq dest
		(cons (cons (car cell)
			    (list (cons 'body
					(if (consp (cdr cell))
					    (mapvector (lambda (item)
							 (list (cons 'body item)))
						       (cdr cell))
					  (vector (list (cons 'body (cdr cell))))))))
		      dest))
	  ))))
    (dolist (cell metadata-features)
      (when (setq fcell (assq (nth 1 cell) dest))
	(if (setq item-num (nth 2 cell))
	    (if (and (setq body-vec (cdr (assq 'body (cdr fcell))))
		     (setq item-num (1- item-num))
		     (<= 0 item-num)
		     (< item-num (length body-vec)))
		(aset body-vec item-num (cons (cons (nth 3 cell) (car cell))
					      (aref body-vec item-num))))
	  (setcdr fcell (cons (cons (nth 3 cell)
				    (car cell))
			      (cdr fcell))))))
    dest))

(defun chise-json-separate-normalized-spec (normalized-spec &optional subnode)
  (let (CCS-spec
	decomposition-spec
	structure-spec
	parents-spec children-spec
	relation-spec
	other-spec
	ret
	base-name domain)
  (dolist (cell normalized-spec)
    (cond
     ((find-charset (car cell))
      (setq CCS-spec (cons cell CCS-spec))
      )
     ((and (not subnode)
	   (memq (car cell) '(<-denotational <-subsumptive)))
      (setq parents-spec (cons (cons (cons (car cell) nil)
				     (cdr cell))
			       parents-spec))
      )
     (t
      (if (setq ret (chise-feature-name-split-domain (car cell)))
	  (setq base-name (car ret)
		domain (cdr ret))
	(setq base-name (car cell)
	      domain nil))
      (cond
       ((eq base-name '=decomposition)
	(setq decomposition-spec (cons (cons domain (cdr cell))
				       decomposition-spec))
	)
       ((eq base-name 'ideographic-structure)
	(setq structure-spec (cons (cons domain (cdr cell))
				   structure-spec))
	)
       ((memq base-name '(<-denotational
			  <-subsumptive))
	(setq parents-spec (cons (cons (cons base-name domain)
				       (cdr cell))
				 parents-spec))
	)
       ((memq base-name '(->denotational
			  ->subsumptive))
	(setq children-spec (cons (cons (cons base-name domain)
					(cdr cell))
				  children-spec))
	)
       ((string-match "^\\(->\\|<-\\)" (symbol-name base-name))
	(setq relation-spec (cons (cons (cons base-name domain)
					(cdr cell))
				  relation-spec))
	)
       (t
	(setq other-spec (cons (cons (cons base-name domain)
				     (cdr cell))
			       other-spec))
	)))))
  (vector parents-spec
	  CCS-spec
	  decomposition-spec
	  structure-spec
	  children-spec
	  relation-spec
	  other-spec)))

(defun chise-json-normalized-feature< (a b)
  (cond
   ((string< (caar a) (caar b))
    )
   ((eq (caar a) (caar b))
    (if (cdar a)
	(if (cdar b)
	    (string< (cdar a) (cdar b))
	  nil)
      t))))

(defun chise-json-list-add-prefix (sequence prefix)
  (if sequence
      (mapvector (lambda (item)
		   (format "%s:%s" prefix item)) sequence)))

(defun chise-json-other-spec-get-radical-strokes (other-spec)
  (let (domain-rs-alist
	radical
	rest ret domain sources)
    (dolist (cell other-spec)
      (cond
       ((eq (caar cell) 'ideographic-radical)
	(setq domain (cdar cell)
	      radical (cdr (assq 'body (cdr cell)))
	      sources (chise-json-list-add-prefix (cdr (assq 'sources (cdr cell)))
						  'bib))
	(if (setq ret (assq domain domain-rs-alist))
	    (setcdr ret (nconc (list* (cons 'kangxi-radical-number radical)
				      (if sources
					  (cons
					   (cons 'kangxi-radical-source sources)
					   (del-alist 'sources
						      (del-alist 'body (cdr cell))))
					(del-alist 'body (cdr cell))))
			       (cdr ret)))
	  (setq domain-rs-alist
		(cons (cons domain
			    (list* (cons 'kangxi-radical-number radical)
				   (if sources
				       (cons
					(cons 'kangxi-radical-source sources)
					(del-alist 'sources
						   (del-alist 'body (cdr cell))))
				     (del-alist 'body (cdr cell)))))
		      domain-rs-alist)))
	)
       ((eq (caar cell) 'ideographic-strokes)
	(setq domain (cdar cell)
	      sources (chise-json-list-add-prefix (cdr (assq 'sources (cdr cell)))
						  'bib))
	(if (setq ret (assq domain domain-rs-alist))
	    (setcdr ret (nconc (list* (cons 'kangxi-strokes
					    (cdr (assq 'body (cdr cell))))
				      (if sources
					  (cons
					   (cons 'kangxi-strokes-source sources)
					   (del-alist 'sources
						      (del-alist 'body (cdr cell))))
					(del-alist 'body (cdr cell))))
			       (cdr ret)))
	  (setq domain-rs-alist
		(cons (cons domain
			    (list* (cons 'kangxi-strokes
					 (cdr (assq 'body (cdr cell))))
				   (if sources
				       (cons
					(cons 'kangxi-strokes-source sources)
					(del-alist 'sources
						   (del-alist 'body (cdr cell))))
				     (del-alist 'body (cdr cell)))))
		      domain-rs-alist)))
	)
       ((eq (caar cell) 'total-strokes)
	(setq domain (cdar cell)
	      sources (chise-json-list-add-prefix (cdr (assq 'sources (cdr cell)))
						  'bib))
	(if (setq ret (assq domain domain-rs-alist))
	    (setcdr ret (nconc (list* (cons 'total-strokes
					    (cdr (assq 'body (cdr cell))))
				      (if sources
					  (cons
					   (cons 'total-strokes-source sources)
					   (del-alist 'sources
						      (del-alist 'body (cdr cell))))
					(del-alist 'body (cdr cell))))
			       (cdr ret)))
	  (setq domain-rs-alist
		(cons (cons domain
			    (list* (cons 'total-strokes
					 (cdr (assq 'body (cdr cell))))
				   (if sources
				       (cons
					(cons 'total-strokes-source sources)
					(del-alist 'sources
						   (del-alist 'body (cdr cell))))
				     (del-alist 'body (cdr cell)))))
		      domain-rs-alist)))
	)
       (t
	(setq rest (cons cell rest))
	)))
    (cons (mapcar (lambda (dspec)
		    (cons (car dspec)
			  (sort (cdr dspec)
				(lambda (a b)
				  (string< (car a)(car b))))))
		  domain-rs-alist)
	  rest)))

(defun chise-json-make-ja-romaji-spec (romaji)
  (let (romaji-for-kana)
    (cond
     ((string-match "-\\([a-z]\\)\\+" romaji)
      (setq romaji-for-kana (list (substring romaji 0 (match-beginning 0))
				  (concat (match-string 1 romaji)
					  (substring romaji (match-end 0)))))
      )
     ((string-match "-\\([a-z]+\\)\\+\\([a-z]\\)5" romaji)
      (setq romaji-for-kana (list (substring romaji 0 (match-beginning 0))
				  (concat
				   (match-string 1 romaji)
				   (match-string 2 romaji)
				   "u")
				  (concat
				   (match-string 2 romaji)
				   "a行5\u6BB5")))
      )
     ((string-match "-\\([a-z]+\\)\\+" romaji)
      (setq romaji-for-kana (list (substring romaji 0 (match-beginning 0))
				  (concat
				   (match-string 1 romaji)
				   (substring romaji (match-end 0)))))
      )
     ((string-match "-" romaji)
      (setq romaji-for-kana (list (substring romaji 0 (match-beginning 0))
				  (substring romaji (match-end 0))))
      )
     (t
      (setq romaji-for-kana (list romaji))))
    (list (cons 'ja-romaji romaji)
	  (cons 'ja-kana
		(concat
		 (historical-roman-to-modern-kana-translate-string
		  (car romaji-for-kana))
		 (if (nth 1 romaji-for-kana)
		     (concat "\u2010"
			     (historical-roman-to-modern-kana-translate-string
			      (nth 1 romaji-for-kana)))
		   "")
		 (if (nth 2 romaji-for-kana)
		     (concat "\u3014"
			     (japanese-katakana
			      (historical-roman-to-modern-kana-translate-string
			       (nth 2 romaji-for-kana)))
			     "\u3015")
		   "")))
	  (cons 'ja-kana-historical
		(concat
		 (historical-roman-to-historical-kana-translate-string
		  (car romaji-for-kana))
		 (if (nth 1 romaji-for-kana)
		     (concat "\u2010"
			     (historical-roman-to-historical-kana-translate-string
			      (nth 1 romaji-for-kana)))
		   "")
		 (if (nth 2 romaji-for-kana)
		     (concat "\u3014"
			     (japanese-katakana
			      (historical-roman-to-historical-kana-translate-string
			       (nth 2 romaji-for-kana)))
			     "\u3015")
		   ""))))))

(defun chise-json-other-spec-get-sound (other-spec)
  (let (domain-sound-alist
	rest ret domain
	kan-list go-list on-list kan go
	kan-spec go-spec i len len-kan len-go)
    (dolist (cell other-spec)
      (cond
       ((eq (caar cell) 'sound)
	(setq domain (cdar cell))
	(cond
	 ((eq domain 'ja/on/kan)
	  (if (null (setq ret (assoc '(sound . ja/on/go) other-spec)))
	      (setq domain-sound-alist
		    (cons (cons 'chise:domain/ja/on
				(mapvector (lambda (item)
					     (cdr (assq 'body item)))
					   (cdr (assq 'body (cdr cell)))))
			  domain-sound-alist))
	    (setq kan-list (cdr (assq 'body (cdr cell)))
		  go-list (cdr (assq 'body (cdr ret)))
		  on-list nil)
	    (setq i 0
		  len-kan (length kan-list)
		  len-go (length go-list)
		  len (max len-kan len-go))
	    (while (< i len)
	      (setq kan (if (< i len-kan)
			    (cdr (assq 'body (aref kan-list i))))
		    go  (if (< i len-go)
			    (cdr (assq 'body (aref go-list i))))
		    i (1+ i))
	      (setq kan-spec
		    (if kan
			(cons 'ja-kan-on (chise-json-make-ja-romaji-spec kan))))
	      (setq go-spec
		    (if go
			(cons 'ja-go-on (chise-json-make-ja-romaji-spec go))))
	      (if (or kan-spec go-spec)
		  (setq on-list
			(cons (if kan-spec
				  (list* kan-spec
					 (if go-spec
					     (list go-spec)))
				(list go-spec))
			      on-list))))
	    (setq domain-sound-alist
		  (cons (cons 'chise:domain/ja/on
			      (apply #'vector on-list))
			domain-sound-alist))
	    ))
	 ((eq domain 'ja/on/go)
	  )
	 ((memq domain '(ja/on ja/on/conventional ja/kun ja/kun/name))
	  (setq domain-sound-alist
		(cons (cons (intern (format "chise:domain/%s" domain))
			    (mapvector (lambda (item)
					 (nconc
					  (chise-json-make-ja-romaji-spec
					   (cdr (assq 'body item)))
					  (del-alist 'body item)))
				       (cdr (assq 'body (cdr cell)))))
		      domain-sound-alist))
	  )
	 ((eq domain 'rep-char/guangyun/Web-yunzi)
	  (setq domain-sound-alist
		(cons
		 (cons 'chise:domain/guangyun
		       (mapvector
			(lambda (item)
			  (setq ret (cdr (assq 'body item)))
			  (setq ret (char-to-string (decode-char '=ucs ret)))
			  (list (cons 'title (format "Web-yunzi: Guangyun-%s" ret))
				(cons 'url
				      (format
				       "http://suzukish.s252.xrea.com/search/inkyo/yunzi/%s"
				       ret))))
			(cdr (assq 'body (cdr cell)))))
		 domain-sound-alist))
	  )
	 (t
	  (setq domain-sound-alist
		(cons (cons (format "chise:domain/%s" domain)
			    (mapvector (lambda (item)
					 (cdr (assq 'body item)))
				       (cdr (assq 'body (cdr cell)))))
		      domain-sound-alist))
	  ))
	)
       (t
	(setq rest (cons cell rest)))))
    (cons domain-sound-alist rest)))

(defun chise-json-char-get-CCS-spec (char &optional spec)
  (unless spec
    (setq spec (char-attribute-alist char)))
  (let (dest)
    (dolist (cell spec)
      (when (find-charset (car cell))
	(setq dest (cons cell dest))))
    (sort dest (lambda (a b)
		 (char-attribute-name< (car a)(car b))))))

(defun chise-json-make-char-id (ccs code-point)
  (let ((ccs-spec (chise-split-ccs-name ccs))
	ccs-base granularity domain)
    (setq ccs-base (pop ccs-spec)
	  granularity (pop ccs-spec)
	  domain (pop ccs-spec))
    (format "chise:ccs/%s/%s/%s/%s"
	    ccs-base
	    (chise-ccs-format-code-point ccs-base code-point "0x" ccs)
	    granularity
	    (or domain "common"))))

(defun chise-json-char-get-id (char)
  (let ((char-CCS-spec (chise-json-char-get-CCS-spec char)))
    (chise-json-make-char-id (caar char-CCS-spec)(cdar char-CCS-spec))))

(defun chise-json-abstract-component-get-description (char)
  (let (ret dest)
    (unless (encode-char char '=ucs)
      (when (or (encode-char char '=>ucs@component)
		(encode-char char '=>ucs@iwds-1)
		(encode-char char '=>ucs@iwds-1/normalized)
		(encode-char char '=>iwds-1)
		(encode-char char '=>ucs@cognate))
	(dolist (child (union
			(get-char-attribute char '->denotational@component)
			(get-char-attribute char '->denotational)))
	  (if (setq ret (chise-json-abstract-component-get-description child))
	      (setq dest (union dest ret))
	    (setq dest (cons child dest))))
	dest))))

(defvar chise-json-ccs-display-priority-alist
  '((ucs             .  0)
    (ucs@jis         .  1)
    (ucs@JP          .  1)
    (ucs@JP/hanazono .  1)
    (gt		     .  2)
    (mj              .  3)
    (hanyo-denshi/ja . 11)
    (hanyo-denshi/jb . 12)
    (hanyo-denshi/jc . 13)
    (hanyo-denshi/jd . 14)
    (hanyo-denshi/ft . 15)
    (hanyo-denshi/ia . 16)
    (hanyo-denshi/ib . 17)
    (hanyo-denshi/hg . 18)
    (hanyo-denshi/ip . 19)
    (hanyo-denshi/jt . 20)
    (hanyo-denshi/ks . 21)
    (hanyo-denshi/tk . 22)
    (daijiten	     . 23)
    (cns11643-1	     . 31)
    (cns11643-2	     . 32)
    (cns11643-3	     . 33)
    (cns11643-4	     . 34)
    (cns11643-5	     . 35)
    (cns11643-6	     . 36)
    (cns11643-7	     . 37)
    (adobe-japan1-0  . 40)
    (adobe-japan1-1  . 41)
    (adobe-japan1-2  . 42)
    (adobe-japan1-3  . 43)
    (adobe-japan1-4  . 44)
    (adobe-japan1-5  . 45)
    (adobe-japan1-6  . 46)
    (big5-cdp	     . 47)
    (gb2312          . 50)
    (gb12345         . 51)
    (jis-x0208@1990  . 60)
    (jis-x0212	     . 61)
    (cbeta	     . 62)
    (jis-x0208@1997  . 63)
    (jis-x0208@1978  . 64)
    (jis-x0208@1983  . 65)
    (ruimoku-v6	     . 70)
    (zinbun-oracle   . 71)
    (shuowen-jiguge4 . 72)
    (shuowen-jiguge5 . 73)
    (shuowen-jiguge  . 73)
    (gt-k	     . 74)
    (hanziku-1	    . 101)
    (hanziku-2      . 102)
    (hanziku-3	    . 103)
    (hanziku-4	    . 104)
    (hanziku-5      . 105)
    (hanziku-6	    . 106)
    (hanziku-7	    . 107)
    (hanziku-8      . 108)
    (hanziku-9      . 109)
    (hanziku-10	    . 110)
    (hanziku-11	    . 111)
    (hanziku-12     . 112)
    (big5	    . 120)
    (daikanwa	    . 121)
    (jef-china3	    . 122)
    (ucs@unicode    . 123)
    (ucs@ks	    . 124)
    ))

(defun chise-json-ccs-get-display-priority (ccs &optional ccs-info)
  (if (eq ccs '=ucs)
      0
    (unless ccs-info
      (setq ccs-info (chise-split-ccs-name ccs)))
    (if (string-match "-\\(itaiji\\|var\\)-\\([0-9][0-9][0-9]\\)$"
		      (symbol-name (car ccs-info)))
	1
      (let ((ret (assq (if (nth 2 ccs-info)
			   (intern (format "%s@%s"
					   (car ccs-info) 
					   (nth 2 ccs-info)))
			 (car ccs-info) )
		       chise-json-ccs-display-priority-alist)))
	(if ret
	    (cdr ret)
	  10000)))))

(defun chise-json-ccs-display-priority< (ccs1 ccs2)
  (let ((p1 (chise-json-ccs-get-display-priority ccs1))
	(p2 (chise-json-ccs-get-display-priority ccs2)))
    (cond
     ((< p1 p2)
      )
     ((= p1 p2)
      (char-attribute-name< ccs1 ccs2)
      ))))

(defun chise-json-char-get-title-info (char)
  (let ((code (encode-char char '=>iwds-1))
	plane subcode gi
	CCS-specs
	ccs-info
	cell ret granularity
	dest)
    (cond
     ((eq code 65535)
      (list "〈八/\u4E37/儿/几/凡/卂/丸/九/尢/尤/兀/丌〉" nil 'abstract-character)
      )
     ((setq ret (chise-json-abstract-component-get-description char))
      (list (concat "〈"
		    (mapconcat (lambda (ch)
				 (cond ((encode-char ch '=ucs)
					(char-to-string ch)
					)
				       ((setq code (char-ucs ch))
					(char-to-string (decode-char '=ucs code))
					)
				       (t
					"?")))
			       ret "/")
		    "〉")
	    nil 'abstract-character)
      )
     (t
      (setq CCS-specs
	    (sort (chise-json-char-get-CCS-spec char)
		  (lambda (a b)
		    (chise-json-ccs-display-priority< (car a)(car b)))))
      (while (and CCS-specs
		  (setq cell (pop CCS-specs))
		  (null
		   (and
		    (setq code (cdr cell))
		    (setq dest
			  (cond
			   ((eq (car cell) '=ucs)
			    (setq granularity
				  (or (nth 1 (chise-split-ccs-name (caar CCS-specs)))
				      'abstract-character))
			    (list (format (if (eq granularity 'abstract-character)
					      "〈%c〉"
					    "%c")
					  char)
				  (format "%s/u%04x.svg"
					  chise-wiki-glyphwiki-glyph-image-url
					  code)
				  granularity)
			    )
			   ((eq (car cell) '==>ucs@bucs)
			    (list (format "〔%c〕"
					  (decode-char '=ucs code))
				  (format "%s/u%04x.svg"
					  chise-wiki-glyphwiki-glyph-image-url
					  code)
				  'super-abstract-character)
			    )
			   ((and
			     (setq ccs-info (chise-split-ccs-name (car cell)))
			     (cond
			      ((eq (car ccs-info) 'gt)
			       (list nil
				     (format "%s?char=GT-%05d"
					     chise-wiki-glyph-cgi-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'gt-k)
			       (list nil
				     (format "%s?char=GT-K%05d"
					     chise-wiki-glyph-cgi-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'big5)
			       (list nil
				     (format "%s?char=B-%04X"
					     chise-wiki-glyph-cgi-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'mj)
			       (list nil
				     (format
				      "https://moji.or.jp/mojikibansearch/img/MJ/MJ%06d.png"
				      code)
				     (nth 1 ccs-info))
			       )
			      ((string-match
				"hanyo-denshi/\\(ja\\|jb\\|jc\\|jd\\|ft\\|ia\\|ib\\|hg\\)"
				(symbol-name (car ccs-info)))
			       (list nil
				     (format
				      "%s/IVD/HanyoDenshi/%s%02d%02d.png"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (upcase (match-string 1 (symbol-name (car ccs-info))))
				      (- (lsh code -8) 32)
				      (- (logand code 255) 32))
				     (nth 1 ccs-info))
			       )
			      ((string-match "hanyo-denshi/\\(ip\\|jt\\)"
					     (symbol-name (car ccs-info)))
			       (list nil
				     (format
				      "%s/IVD/HanyoDenshi/%s%04X.png"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (upcase (match-string 1 (symbol-name (car ccs-info))))
				      code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'hanyo-denshi/ks)
			       (list nil
				     (format
				      "%s/IVD/HanyoDenshi/KS%06d.png"
				      chise-wiki-legacy-bitmap-glyphs-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'hanyo-denshi/tk)
			       (list nil
				     (format
				      "%s/IVD/HanyoDenshi/TK%08d.png"
				      chise-wiki-legacy-bitmap-glyphs-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'daijiten)
			       (list nil
				     (format
				      "%s/%05d.png"
				      chise-wiki-daijiten-bitmap-glyphs-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((string-match "cns11643-\\([1-7]\\)"
					     (symbol-name (car ccs-info)))
			       (list nil
				     (format
				      "%s/CNS%s/%04X.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (match-string 1 (symbol-name (car ccs-info)))
				      code)
				     (nth 1 ccs-info))
			       )
			      ((string-match "adobe-japan1"
					     (symbol-name (car ccs-info)))
			       (list nil
				     (format
				      "%s/IVD/AdobeJapan1/CID+%d.png"
				      chise-wiki-legacy-bitmap-glyphs-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'big5-cdp)
			       (list nil
				     (format
				      "%s?char=CDP-%04X"
				      chise-wiki-glyph-cgi-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'gb2312)
			       (list nil
				     (format
				      "%s/GB0/%02d-%02d.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (- (lsh code -8) 32)
				      (- (logand code 255) 32))
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'gb12345)
			       (list nil
				     (format
				      "%s/GB1/%02d-%02d.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (- (lsh code -8) 32)
				      (- (logand code 255) 32))
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'jis-x0208)
				    (memq (nth 2 ccs-info) '(1990 1997)))
			       (list nil
				     (format
				      "%s/JIS-90/%02d-%02d.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (- (lsh code -8) 32)
				      (- (logand code 255) 32))
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'jis-x0212)
			       (list nil
				     (format
				      "%s/JSP/%02d-%02d.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (- (lsh code -8) 32)
				      (- (logand code 255) 32))
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'ucs)
				    (memq (nth 2 ccs-info) '(iso)))
			       (list nil
				     (format
				      "%s/u%04x.svg"
				      chise-wiki-glyphwiki-glyph-image-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'ucs)
				    (memq (nth 2 ccs-info) '(ks)))
			       (list nil
				     (format
				      "%s/u%04x-k.svg"
				      chise-wiki-glyphwiki-glyph-image-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'cbeta)
			       (list nil
				     (format
				      "%s/cb-gaiji/%02d/CB%05d.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (/ code 1000) code)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'jis-x0208)
				    (eq (nth 2 ccs-info) 1978))
			       (list nil
				     (format
				      "%s/JIS-78/%02d-%02d.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (- (lsh code -8) 32)
				      (- (logand code 255) 32))
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'jis-x0208)
				    (eq (nth 2 ccs-info) 1983))
			       (list nil
				     (format
				      "%s/JIS-83/%02d-%02d.gif"
				      chise-wiki-legacy-bitmap-glyphs-url
				      (- (lsh code -8) 32)
				      (- (logand code 255) 32))
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'ruimoku-v6)
			       (list nil
				     (format
				      "%s?char=RUI6-%04X"
				      chise-wiki-glyph-cgi-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'zinbun-oracle)
			       (list nil
				     (format "%s/ZOB-1968/%04d.png"
					     chise-wiki-legacy-bitmap-glyphs-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'shuowen-jiguge4)
			       (list nil
				     (format "%s/ShuoWen/Jiguge4/%05d.png"
					     chise-wiki-legacy-bitmap-glyphs-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((memq (car ccs-info) '(shuowen-jiguge shuowen-jiguge5))
			       (list nil
				     (format "%s/ShuoWen/Jiguge5/%05d.png"
					     chise-wiki-legacy-bitmap-glyphs-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((string-match
				"^ucs-\\(itaiji\\|var\\)-\\([0-9][0-9][0-9]\\)$"
				(symbol-name (car ccs-info)))
			       (list nil
				     (format
				      "%s/u%04x-%s-%s.svg"
				      chise-wiki-glyphwiki-glyph-image-url
				      code
				      (match-string 1 (symbol-name (car ccs-info)))
				      (match-string 2 (symbol-name (car ccs-info))))
				     (nth 1 ccs-info))
			       )
			      ((string-match
				"^big5-cdp-\\(itaiji\\|var\\)-\\([0-9][0-9][0-9]\\)$"
				(symbol-name (car ccs-info)))
			       (list nil
				     (format
				      "%s/cdp-%04x-%s-%s.svg"
				      chise-wiki-glyphwiki-glyph-image-url
				      code
				      (match-string 1 (symbol-name (car ccs-info)))
				      (match-string 2 (symbol-name (car ccs-info))))
				     (nth 1 ccs-info))
			       )
			      ((string-match
				"^hanziku-\\([0-9]+\\)$"
				(symbol-name (car ccs-info)))
			       (list nil
				     (format
				      "%s?char=HZK%02d-%04X"
				      chise-wiki-glyph-cgi-url
				      (string-to-number
				       (match-string 1 (symbol-name (car ccs-info))))
				      code)
				     (nth 1 ccs-info))
			       )
			      ((and (string-match
				     "^hng-"
				     (symbol-name (car ccs-info)))
				    (setq ret (charset-registry (car cell)))
				    (string-match "^hng-\\([0-9]+\\)" ret)
				    (setq plane (string-to-number (match-string 1 ret))))
			       (setq subcode (% code 10)
				     code (/ code 10))
			       (setq subcode
				     (if (eq subcode 0)
					 ""
				       (char-to-string (decode-char 'ascii (+ 96 subcode)))))
			       (list nil
				     (format
				      "%s/%03d/%04d%s.png"
				      chise-wiki-hng-bitmap-glyphs-url
				      plane code subcode)
				     (nth 1 ccs-info))
			       )
			      ((eq (car ccs-info) 'jef-china3)
			       (list nil
				     (format "%s/JEF-CHINA3/%04X.png"
					     chise-wiki-bitmap-glyph-image-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'ucs)
				    (eq (nth 2 ccs-info) 'unicode))
			       (list nil
				     (format
				      "https://www.unicode.org/cgi-bin/refglyph?24-%04X"
				      code)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'ucs)
				    (eq (nth 2 ccs-info) 'JP/hanazono))
			       (list nil
				     (format "%s/u%04x.svg"
					     chise-wiki-glyphwiki-glyph-image-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'daikanwa)
				    (memq (nth 2 ccs-info) '(nil rev2)))
			       (list nil
				     (format
				      "%s/dkw-%05d.svg"
				      chise-wiki-glyphwiki-glyph-image-url
				      code)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'ucs)
				    (eq (nth 2 ccs-info) 'JP/hanazono))
			       (list nil
				     (format "%s/u%04x.svg"
					     chise-wiki-glyphwiki-glyph-image-url
					     code)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'chise-hdic-tsj)
				    (setq ret
					  (or (get-char-attribute char '=hdic-tsj-glyph-id)
					      (and (setq gi (decode-char
							     '===chise-hdic-tsj code))
						   (get-char-attribute
						    gi '=hdic-tsj-glyph-id)))))
			       (list nil
				     (format "https://viewer.hdic.jp/img/tsj/%s.jpg"
					     ret)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'chise-hdic-syp)
				    (setq ret
					  (or (get-char-attribute char '=hdic-syp-entry-id)
					      (and (setq gi (decode-char
							     '===chise-hdic-syp code))
						   (get-char-attribute
						    gi '=hdic-syp-entry-id)))))
			       (list nil
				     (format "https://viewer.hdic.jp/img/syp/%s"
					     ret)
				     (nth 1 ccs-info))
			       )
			      ((and (eq (car ccs-info) 'chise-hdic-ktb)
				    (setq ret
					  (or (get-char-attribute char '=hdic-ktp-entry-id)
					      (and (setq gi (decode-char
							     '===chise-hdic-ktb code))
						   (get-char-attribute
						    gi '=hdic-ktb-entry-id)))))
			       (list nil
				     (format "https://hdic.chise.org/img/ktb/%s.jpg"
					     ret)
				     (nth 1 ccs-info))
			       )
			      			      
			      )
			     ))))
		    ))))
      dest))))

(defun chise-json-make-char-ref-object (id title-info name)
  (let (title image dest)
    (setq dest
	  (list* (cons 'granularity (format "chise:glyph-granularity/%s" (nth 2 title-info)))
		 (if (setq image (nth 1 title-info))
		     (list (cons 'image image)))))
    (if (setq title (car title-info))
	(setq dest (cons (cons 'title title)
			 dest)))
    (if name
	(setq dest (cons (cons 'name name)
			 dest)))
    (cons (cons '@id id)
	  dest)))

(defun chise-json-char-get-ref-info (char)
  (chise-json-make-char-ref-object (chise-json-char-get-id char)
				   (chise-json-char-get-title-info char)
				   (get-char-attribute char 'name)))

(defun chise-json-dump-get-path (id)
  (let (len path ccs code ccs-obj dim)
    (setq path
	  (if (string-match "^chise:" id)
	      (substring id (match-end 0))
	    id))
    (cond
     ((string-match "^ccs/\\(.*\\)/0x\\([0-9A-F]+\\)/" path)
      (setq ccs (match-string 1 path)
	    code (match-string 2 path))
      (setq dim
	    (if (setq ccs-obj (or (find-charset (intern ccs))
				  (find-charset (intern (concat "=" ccs)))))
		(charset-dimension ccs-obj)
	      2))
      (setq len (length code))
      (cond
       ((eq dim 1)
	path)
       ((eq dim 2)
	(setq path (format "ccs/%s/SB0x%02s/%02s/%s"
			   ccs
			   (substring code 0 (max (- len 2) 0))
			   (substring code (max (- len 2) 0) len)
			   (substring path (match-end 0))))
	)
       (t
	(setq path (format "ccs/%s/ST0x%01s/%02s/%02s/%s"
			   ccs
			   (substring code 0 (max (- len 4) 0))
			   (substring code (max (- len 4) 0) (max  (- len 2) 0))
			   (substring code (max (- len 2) 0) len)
			   (substring path (match-end 0))))
	))
      )
     ((string-match "^ccs/\\(daikanwa\\|mj\\|koseki\\|gt\\|gt-k\\)/\\([0-9]+\\)/" path)
      (setq ccs (match-string 1 path)
	    code (match-string 2 path))
      (setq len (length code))
      (setq path (format "ccs/%s/SB_%02s/%03s/%s"
			 ccs
			 (substring code 0 (max (- len 3) 0))
			 (substring code (max (- len 3) 0) len)
			 (substring path (match-end 0))))
      )
     (t
      path))))

(defun chise-json-char-get-info (char &optional subnode without-CCS-spec)
  (let ((json-encoding-pretty-print t)
	(coding-system-for-write 'utf-8-mcs-er)
	(char-spec (char-attribute-alist char))
	char-info
	(title-info (chise-json-char-get-title-info char))
	name general-category bidi-category mirrored
	hanyu-dazidian
	parents-spec ; domain-parents-alist
	CCS-spec
	decomposition-spec decomposition-type
	structure-spec
	children-spec
	relation-spec
	other-spec
	radical-strokes-spec
	sound-spec
	id
	ret i flag
	node-id
	anno-id
	domain code
	kangxi-radical kangxi-strokes total-strokes
	reldesc sources
	dest rest)
    (setq char-info (chise-json-separate-normalized-spec
		     (chise-json-char-get-normalized-spec char subnode char-spec)
		     subnode)
	  name (cdr (assq 'name char-spec))
	  general-category (cdr (assq 'general-category char-spec))
	  bidi-category (cdr (assq 'bidi-category char-spec))
	  mirrored (assq 'mirrored char-spec)
	  hanyu-dazidian (cdr (assq 'hanyu-dazidian char-spec)))
    (setq parents-spec   (aref char-info 0)
	  CCS-spec       (sort (aref char-info 1)
			       (lambda (a b)
				 (char-attribute-name< (car a)(car b))))
	  decomposition-spec (aref char-info 2)
	  structure-spec (aref char-info 3)
	  children-spec  (aref char-info 4)
	  relation-spec  (aref char-info 5)
	  other-spec     (aref char-info 6))
    (setq id (chise-json-make-char-id (caar CCS-spec)
				      (cdr (assq 'body (cdar CCS-spec)))))
    (setq ret (chise-json-other-spec-get-radical-strokes other-spec))
    (setq radical-strokes-spec (car ret)
	  other-spec (cdr ret))
    (setq ret (chise-json-other-spec-get-sound other-spec))
    (setq sound-spec (car ret)
	  other-spec (cdr ret))
    (dolist (cell (sort children-spec
			(lambda (a b)
			  (chise-json-normalized-feature< b a))))
      (setq flag nil)
      (setq ret (cdr (assq 'body (cdr cell))))
      (setq dest
	    (cons
	     (cons (cond
		    ((equal (car cell) '(->denotational))
		     (setq flag t)
		     'denotation)
		    ((equal (car cell) '(->subsumptive))
		     (setq flag t)
		     'subsume)
		    ((equal (car cell) '(->denotational . usage))
		     'unified-in-usage)
		    ((equal (car cell) '(->denotational . component))
		     'unified-as-component)
		    ((null (cdar cell))
		     (caar cell))
		    (t
		     (format "%s@%s" (caar cell)(cdar cell))))
		   (if flag
		       (mapvector (lambda (item)
				    (chise-json-char-get-info
				     (cdr (assq 'body item))
				     'subnode
				     without-CCS-spec))
				  ret)
		     (mapvector (lambda (item)
				  (chise-json-char-get-ref-info
				   (cdr (assq 'body item))))
				ret)
		     ))
	     dest)))
    (when relation-spec
      (setq dest
	    (cons
	     (cons
	      'relations
	      (mapcar (lambda (cell)
			(setq ret (if (cdar cell)
				      (format "%s@%s" (caar cell)(cdar cell))
				    (caar cell)))
			(setq anno-id
			      (format "%s/%s"
				      id (www-uri-encode-feature-name ret)))
			(setq reldesc nil
			      sources nil
			      rest nil)
			(dolist (item (cdr cell))
			  (cond
			   ((eq (car item) 'body)
			    (setq i 0)
			    (setq reldesc
				  (mapvector
				   (lambda (spec)
				     (setq i (1+ i))
				     (cons
				      (cons '@id (format "%s/%d"
							 anno-id i))
				      (mapcar
				       (lambda (feat)
					 (cond
					  ((eq (car feat) 'body)
					   (cons
					    'target
					    (chise-json-char-get-ref-info (cdr feat)))
					   )
					  ((eq (car feat) 'sources)
					   (cons 'sources
						 (chise-json-list-add-prefix
						  (cdr feat) 'bib))
					   )
					  (t feat)))
				       (sort spec
					     (lambda (a b)
					       (string< (car a)(car b)))))))
				   (cdr item)))
			    )
			   ((eq (car item) 'sources)
			    (setq sources (chise-json-list-add-prefix (cdr item) 'bib))
			    )
			   (t
			    (setq rest (cons item rest))
			    )))
			(cons
			 ret
			 (list*
			  (cons '@id anno-id)
			  (cons 'reldesc reldesc)
			  (sort (if sources
				    (cons (cons 'sources sources)
					  rest)
				  rest)
				(lambda (a b)
				  (string< (car a)(car b)))))))
		      (sort relation-spec
			    #'chise-json-normalized-feature<)))
	     dest)))
    (if (and (not without-CCS-spec)
	     CCS-spec)
	(setq dest
	      (cons
	       (cons 'unify
		     (mapcar
		      (lambda (cell)
			(setq code (cdr (assq 'body (cdr cell))))
			(setq ret (chise-split-ccs-name (car cell)))
			(setq node-id (chise-json-make-char-id (car cell) code))
			(setq ret
			      (list*
			       (cons 'CCS (format "ccs:%s" (car ret)))
			       (cons 'granularity (format "chise:glyph-granularity/%s"
							  (nth 1 ret)))
			       (cons 'domain (format "chise:domain/%s"
						     (or (nth 2 ret) "common")))
			       (cons 'code-point code)
			       (del-alist 'body (cdr cell))))
			(cons (car cell)
			      (cons (cons '@id node-id)
				    ret))
			)
		      CCS-spec))
	       dest)))
    (when structure-spec
      (setq dest
	    (cons
	     (cons
	      'structure-descriptions
	      (mapcar (lambda (cell)
			(cons
			 (format "chise:domain/structure/%s" (or (car cell) 'functional))
			 (progn
			   (setq ret (ideographic-structure-compact
				      (mapcar #'identity
					      (cdr (assq 'body (cdr cell))))))
			   (list*
			    (cons 'ids
				  (chise-json-encode-ucs-string
				   (ideographic-structure-to-ids ret)))
			    (cons 'ideographic-structure ret)
			    (del-alist 'body (cdr cell))))))
		      (sort structure-spec
			    (lambda (a b)
			      (if (car a)
				  (if (car b)
				      (string< (car a)(car b))
				    nil)
				t)))))
	     dest)))
    (when decomposition-spec
      (setq dest
	    (cons
	     (cons
	      'decompositions
	      (mapcar (lambda (cell)
			(cons
			 (if (car cell)
			     (progn
			       (setq decomposition-type 'ivs)
			       (format "chise:domain/ivd/%s" (car cell)))
			   (progn
			     (setq decomposition-type 'decomposed-string)
			     'chise:domain/common))
			 (progn
			   (setq ret (mapcar #'identity
					     (cdr (assq 'body (cdr cell)))))
			   (list*
			    (cons decomposition-type
				  (chise-json-encode-ucs-string
				   (mapconcat #'char-to-string ret "")))
			    (cons 'character-sequence
				  (mapvector #'chise-json-char-get-ref-info
					     ret))
			    (del-alist 'body (cdr cell))))))
		      (sort decomposition-spec
			    (lambda (a b)
			      (if (car a)
				  (if (car b)
				      (string< (car a)(car b))
				    nil)
				t)))))
	     dest)))
    (dolist (cell (sort other-spec
			(lambda (a b)
			  (chise-json-normalized-feature< b a))))
      (cond
       ((memq (caar cell) '(composition abstract-glyph ideographic-products))
	)
       ((eq (caar cell) 'daijiten-pages)
	(setq dest
	      (cons
	       (cons 'daijiten-pages
		     (mapvector (lambda (item)
				  (setq ret (cdr (assq 'body item)))
				  (list
				   (cons 'daijiten-page ret)
				   (cons 'url
					 (format "http://image.chise.org/tify/?manifest=https://www.dl.ndl.go.jp/api/iiif/950498/manifest.json&tify={%%22pages%%22:[%d]}"
						 (daijiten-page-number-to-ndl-950498 ret))
					 )))
				(cdr (assq 'body (cdr cell)))))
	       dest))
	)
       ((eq (caar cell) '*references)
	(setq dest (cons (cons 'citation
			       (mapvector
				(lambda (item)
				  (setq ret (cdr (assq 'body item)))
				  (list
				   (cons 'description (nth 2 ret))
				   (cons 'url
					 (plist-get (nth 1 ret) :ref))))
				(cdr (assq 'body (cdr cell)))))
			 dest))
	)
       (t
	(setq dest (cons (cons (if (cdar cell)
				   (intern (format "%s@%s"
						   (caar cell)(cdar cell)))
				 (caar cell))
			       (cdr cell))
			 dest))
	))
      )
    (if hanyu-dazidian
	(setq dest
	      (cons (cons 'hanyu-dazidian hanyu-dazidian)
		    dest)))
    (if mirrored
	(setq dest
	      (cons (cons 'mirrored (or (cdr mirrored) :json-false))
		    dest)))
    (if bidi-category
	(setq dest
	      (cons (cons 'bidi-category bidi-category)
		    dest)))
    (if general-category
	(setq dest
	      (cons (cons 'general-category general-category)
		    dest)))
    (if sound-spec
	(setq dest
	      (cons (cons 'phonetic-value (sort sound-spec
						(lambda (a b)
						  (string< (car a)(car b)))))
		    dest)))
    (if radical-strokes-spec
	(setq dest
	      (cons (cons
		     'radical-and-strokes
		     (sort
		      (mapcar (lambda (cell)
				(setq domain
				      (intern
				       (format "chise:domain/%s" (or (car cell) 'common))))
				(setq cell (cdr cell))
				(setq kangxi-radical nil
				      kangxi-strokes nil
				      total-strokes nil)
				(setq rest
				      (mapcan
				       (lambda (pair)
					 (cond
					  ((eq (car pair) 'kangxi-radical-number)
					   (setq kangxi-radical pair)
					   nil)
					  ((eq (car pair) 'kangxi-strokes)
					   (setq kangxi-strokes pair)
					   nil)
					  ((eq (car pair) 'total-strokes)
					   (setq total-strokes pair)
					   nil)
					  (t
					   (list pair))))
				       cell))
				(unless kangxi-radical
				  (setq kangxi-radical
					(assq 'kangxi-radical-number
					      (cdr (assq nil radical-strokes-spec)))))
				(unless kangxi-strokes
				  (setq kangxi-strokes
					(assq 'kangxi-strokes
					      (cdr (assq nil radical-strokes-spec)))))
				(unless total-strokes
				  (setq total-strokes
					(assq 'total-strokes
					      (cdr (assq nil radical-strokes-spec)))))
				(setq cell rest)
				(if total-strokes
				    (setq cell (cons total-strokes cell)))
				(if kangxi-strokes
				    (setq cell (cons kangxi-strokes cell)))
				(when kangxi-radical
				  (setq cell
					(list*
					 (cons 'kangxi-radical
					       (char-to-string
						(ideographic-radical (cdr kangxi-radical))))
					 kangxi-radical
					 cell)))
				(cons domain cell))
			      radical-strokes-spec)
		      (lambda (a b)
			(if (eq (car a) 'chise:domain/common)
			    nil
			  (if (eq (car b) 'chise:domain/common)
			      t
			    (string< (car a)(car b)))))))
		    dest)))
    (dolist (cell (sort parents-spec
			(lambda (a b)
			  (chise-json-normalized-feature< b a))))
      (setq dest
	    (cons (cons (cond
			 ((equal (car cell) '(<-denotational))
			  'connotation)
			 ((equal (car cell) '(<-denotational . usage))
			  'jishu)
			 ((equal (car cell) '(<-denotational . component))
			  'component-category)
			 ((equal (car cell) '(<-subsumptive))
			  'subsumed-in)
			 ((null (cdar cell))
			  (caar cell))
			 (t
			  (format "%s@%s" (caar cell)(cdar cell))
			  ))
			(mapvector (lambda (item)
				     (chise-json-char-get-ref-info
				      (cdr (assq 'body item))))
				   (cdr (assq 'body (cdr cell)))))
		  dest)))
    (setq dest
	  (append (chise-json-make-char-ref-object id title-info name)
		  dest))
    dest))

(defun chise-json-dump-char (char &optional ccs code-point force-rewrite)
  (let ((json-encoding-pretty-print t)
	(coding-system-for-write 'utf-8-mcs-er)
	id char-CCS-spec
	path-id
	root-char root-id root-info path dir file ret)
    (unless (setq char-CCS-spec (chise-json-char-get-CCS-spec char))
      (cond ((and (setq ret (split-char char))
		  (memq (char-feature-property (car ret) 'mother) '(ucs =ucs =ucs@JP/hanazono))
		  (setq ret (encode-char char '=ucs)))
	     (put-char-attribute char '=ucs ret)
	     )
	    (t
	     (setq char (define-char (list ret)))
	     ))
      (setq char-CCS-spec (chise-json-char-get-CCS-spec char)))
    (setq id (chise-json-make-char-id (caar char-CCS-spec)(cdar char-CCS-spec)))
    (setq path-id (if (and ccs code-point)
		      (chise-json-make-char-id ccs code-point)
		    id))
    (setq root-char (chise-json-char-get-root char))
    (setq path (chise-json-dump-get-path path-id))
    (setq dir (expand-file-name path chise-json-dump-directory))
    (with-temp-buffer
      (unless (file-exists-p dir)
	(make-directory dir t))
      (setq file (expand-file-name "index.jsonld" dir))
      (when (or force-rewrite
		(not (file-exists-p file)))
	(insert
	 (json-encode
	  (list* '(@context . "http://api.chise.org/contexts/v1/chise.jsonld")
		 (chise-json-char-get-info char))))
	(write-region (point-min)(point-max) file))

      (when (and (not (eq char root-char))
		 (setq file (expand-file-name "index_with-root.jsonld" dir))
		 (or force-rewrite
		     (not (file-exists-p file))))
	(setq root-id (chise-json-char-get-id root-char))
	(erase-buffer)
	(insert
	 (json-encode
	  (list '(@context . "http://api.chise.org/contexts/v1/chise.jsonld")
		(cons '$target id)
		(cons '@id root-id))))
	(write-region (point-min)(point-max) file)
	(setq path (chise-json-dump-get-path root-id))
	(setq dir (expand-file-name path chise-json-dump-directory))
	(unless (file-exists-p dir)
	  (make-directory dir t))
	(setq file (expand-file-name "index.jsonld" dir))
	(when (or force-rewrite
		  (not (file-exists-p file)))
	  (erase-buffer)
	  (setq root-info (chise-json-char-get-info root-char))
	  (insert
	   (json-encode
	    (list* '(@context . "http://api.chise.org/contexts/v1/chise.jsonld")
		   root-info)))
	  (write-region (point-min)(point-max) file))))))

(defun chise-json-dump-ccs (ccs)
  (map-char-attribute
   (lambda (char val)
     ;; (chise-json-dump-char char ccs val)
     (chise-json-dump-char char)
     nil)
   ccs))

(defun chise-json-dump-ucs-basic ()
  (let ((code #x4E00)
	char)
    (while (<= code #x9FEA)
      (when (setq char (decode-char '=ucs code))
	(chise-json-dump-char char))
      (setq code (1+ code)))))

(defun chise-json-dump-big5-cdp ()
  (map-char-attribute
   (lambda (char val)
     (if (and (<= #x854B val)(<= val #x8DFE))
	 ;; (chise-json-dump-char char ccs val)
	 (chise-json-dump-char char))
     nil)
   '=big5-cdp))

(defun chise-json-api-character-get-info (args)
  (let ((coding-system-for-read 'utf-8-mcs-er)
	char id char-CCS-spec
	root-char root-id
	dest ret
	path dir file)
    (when (and (setq char (assoc "character" args))
	       (setq char (cdr char))
	       (setq char (or (chise-rdf-iri-decode-object char)
			      (www-uri-decode-object 'character char))))
      (unless (setq char-CCS-spec (chise-json-char-get-CCS-spec char))
	(cond ((and (setq ret (split-char char))
		    (memq (char-feature-property (car ret) 'mother) '(ucs =ucs))
		    (setq ret (encode-char char '=ucs)))
	       (put-char-attribute char '=ucs ret)
	       )
	      (t
	       (setq char (define-char (list ret)))
	       ))
	(setq char-CCS-spec (chise-json-char-get-CCS-spec char)))
      (setq root-char (chise-json-char-get-root char))
      (setq id (chise-json-make-char-id (caar char-CCS-spec)(cdar char-CCS-spec)))
      (setq root-id
	    (if (eq char root-char)
		id
	      (chise-json-char-get-id root-char)))
      (setq path (chise-json-dump-get-path root-id))
      (setq dir (expand-file-name path chise-json-dump-directory))
      (setq file (expand-file-name "index.jsonld" dir))
      (cons
       (if (file-exists-p file)
	   (with-temp-buffer
	     (insert-file-contents-literally file)
	     (when (not (eq char root-char))
	       (goto-char (point-min))
	       (when (search-forward "\"@id\":" nil t)
		 (goto-char (match-beginning 0))
		 (insert (format "\"$target\": \"%s\",\n  "
				 id))))
	     (buffer-substring (point-min)(point-max))
             ;; (encode-coding-string (buffer-substring (point-min)(point-max))
             ;;                       'utf-8-mcs-er)	     
	     )
	 (setq dest (chise-json-char-get-info root-char nil 'without-CCS-spec))
	 (unless (eq char root-char)
	   (setq dest (cons (cons '$target id)
			    dest)))
	 (setq dest
	       (cons '(@context . "http://api.chise.org/contexts/v1/chise.jsonld")
		     dest))
	 (encode-coding-string (json-encode dest) 'utf-8-mcs-er))
       "application/ld+json")
      )))
      
(defun chise-batch-character-api ()
  (setq debug-on-error t)
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((method (pop command-line-args-left))
	     (target (pop command-line-args-left))
	     args
	     ret)
	(cond
	 ((stringp target)
	  (setq args
		(mapcar (lambda (cell)
			  (if (string-match "=" cell)
			      (cons
			       (substring cell 0 (match-beginning 0))
			       (substring cell (match-end 0)))
			    cell))
			(split-string target "&")))
	  (cond
	   ((equal method "decode")
	    (setq ret (chise-json-api-character-decode args))
	    )
	   ((equal method "encode")
	    (setq ret (chise-json-api-character-encode args))
	    )
	   ((equal method "get")
	    (setq ret (chise-json-api-character-get args))
	    )
	   ((equal method "get-info")
	    (setq ret (chise-json-api-character-get-info args))
	    )
           ;; ((equal method "get-spec")
           ;;  (setq ret (chise-json-api-character-get-spec args))
           ;;  )
	   ((equal method "ids-match")
	    (setq ret (chise-json-api-character-ids-match args))
	    ))
	  (cond
	   ((consp ret)
	    (princ (format "Content-Type: %s

%s"
			   (cdr ret)
			   (car ret)))
	    )
	   ((stringp ret)
	    (princ (format "Content-Type: application/json

%s"
			   ret))
	    )
	   (t
	    (princ (format "Content-Type: application/json

null
"))
	    ))
	  )))
    (error nil
	   (princ "Content-Type: text/plain; charset=UTF-8

")
	   (princ (format "{ \"error\": \"%S\" }" err))))
  (princ "\n"))

(defun chise-json-api-object-decode (args)
  (let (feature value genre obj format-type ret)
    (when (and (setq genre (or (assoc "genre" args)
			       (assoc "type" args)))
	       (setq genre (cdr genre))
	       (setq genre (chise-rdf-iri-decode-feature-name genre))
	       (setq feature (assoc "feature" args))
	       (setq feature (cdr feature))
	       (setq feature (chise-rdf-iri-decode-feature-name feature))
	       (setq value (assoc "value" args))
	       (setq value (cdr value)))
      (cond
       ((string-match "^0x" value)
	(setq value (string-to-number (substring value (match-end 0)) 16))
	)
       ;; ((setq ret (read-from-string value))
       ;;  (setq value (car ret))
       ;;  )
       (t
	(setq value (est-uri-decode-feature-name value))
	))
      (if (setq format-type (assoc "format" args))
	  (setq format-type (cdr format-type)))
      (when (setq obj (concord-decode-object feature value genre))
	(cond
	 ((equal format-type "EsT")
	  (encode-coding-string
	   (json-encode
	    (www-uri-encode-object obj))
	   'utf-8-mcs-er)
	  )
	 ((equal format-type "json")
	  (chise-json-encode-object obj)
	  )
	 (t
	  (setq ret (chise-json-encode-object* obj))
	  (cond
	   ((stringp ret)
	    (cons
	     (encode-coding-string
	      (json-encode
	       (list '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     '(@type . genre:character)
		     (cons '@id (format "character:%s" ret))))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    )
	   (t
	    (cons
	     (encode-coding-string
	      (json-encode
	       (cons '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     ret))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    ))))))))

(defun chise-json-api-object-get (args)
  (let (obj feat val)
    (when (and (setq obj (assoc "object" args))
	       (setq obj (cdr obj))
	       (setq obj (chise-rdf-iri-decode-object obj))
	       (setq feat (assoc "feature" args))
	       (setq feat (cdr feat))
	       (setq feat (chise-rdf-iri-decode-feature-name feat)))
      (if (setq val (concord-object-get obj feat))
	  (encode-coding-string (json-encode val) 'utf-8-mcs-er)))))

;; (defun chise-json-api-object-get-spec (args)
;;   (let ((media-type "application/json")
;;         obj ret feature-format)
;;     (when (and (setq obj (assoc "object" args))
;;                (setq obj (cdr obj))
;;                (setq obj (chise-rdf-iri-decode-object obj)))
;;       (when (setq ret (concord-object-spec obj))
;;         (if (setq feature-format (assoc "feature-format" args))
;;             (setq feature-format (cdr feature-format)))
;;         (cons (encode-coding-string
;;                (json-encode
;;                 (cond
;;                  ((equal feature-format "native")
;;                   (sort ret
;;                         (lambda (a b)
;;                           (string< (car a)(car b))))
;;                   )
;;                  ((equal feature-format "json")
;;                   (mapcar (lambda (pair)
;;                             (cons (www-uri-encode-feature-name (car pair))
;;                                   (cdr pair)))
;;                           (sort ret
;;                                 (lambda (a b)
;;                                   (string< (car a)(car b)))))
;;                   )
;;                  (t ; (equal feature-format "json-ld")
;;                   (setq media-type "application/ld+json")
;;                   (concord-object-get-json-ld-spec obj ret)
;;                   )))
;;                'utf-8-mcs-er)
;;               media-type)))))

(defun chise-batch-object-api ()
  (setq debug-on-error t)
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((method (pop command-line-args-left))
	     (target (pop command-line-args-left))
	     args
	     ret)
	(cond
	 ((stringp target)
	  (setq args
		(mapcar (lambda (cell)
			  (if (string-match "=" cell)
			      (cons
			       (substring cell 0 (match-beginning 0))
			       (substring cell (match-end 0)))
			    cell))
			(split-string target "&")))
	  (cond
	   ((equal method "decode")
	    (setq ret (chise-json-api-object-decode args))
	    )
	   ((equal method "get")
	    (setq ret (chise-json-api-object-get args))
	    )
           ;; ((equal method "get-spec")
           ;;  (setq ret (chise-json-api-object-get-spec args))
           ;;  )
	   )
	  (cond
	   ((consp ret)
	    (princ (format "Content-Type: %s

%s"
			   (cdr ret)
			   (car ret)))
	    )
	   ((stringp ret)
	    (princ (format "Content-Type: application/json

%s"
			   ret))
	    )
	   (t
	    (princ (format "Content-Type: application/json

null
"))
	    ))
	  )))
    (error nil
	   (princ "Content-Type: text/plain; charset=UTF-8

")
	   (princ (format "{ \"error\": \"%S\" }" err))))
  (princ "\n"))
