;; -*- coding: utf-8-mcs-er -*-
(require 'cwiki-format)
(require 'json)
(require 'ids-find)

(setq file-name-coding-system 'utf-8-mcs-er)

(make-coding-system
 'utf-16-le-mcs-no-composition 'utf-16
 "Coding-system of UTF-16be without composition."
 '(mnemonic "MTF16le-nc" disable-composition t))

(mount-char-attribute-table 'ideographic-products)


(unless (fboundp 'orig:json-encode)
  (fset 'orig:json-encode (symbol-function #'json-encode)))

(defun chise-json-encode-object* (object)
  (let (ucs ret)
    (cond
     ((characterp object)
      (if (setq ucs (encode-char object '=ucs))
	  (if (<= ucs #xFFFF)
	      (char-to-string object)
	    (setq ret (encode-coding-string (char-to-string object)
					    'utf-16-le-mcs-no-composition))
	    (format "\\u%2X%02X\\u%02X%02X"
		    (aref ret 1)
		    (aref ret 0)
		    (aref ret 3)
		    (aref ret 2)
		    ))
	(list '(@type . genre:character)
	      (cons '@id (chise-rdf-iri-encode-object object))))
      )
     ((concord-object-p object)
      (list (cons '@type (format "genre:%s" (concord-object-genre object)))
	    (cons '@id (chise-rdf-iri-encode-object object)))
      ))))

(defun json-encode (object)
  "Return a JSON representation of OBJECT as a string."
  (if (or (characterp object)
	  (concord-object-p object))
      (setq object (chise-json-encode-object* object)))
  (orig:json-encode object))

(defun chise-json-encode-object (object)
  (encode-coding-string (json-encode object) 'utf-8-mcs-er))


(defun chise-rdf-iri-make-ccs@domain (base context)
  (let ((base-iri (char-feature-property base '=json-ld-item)))
    (unless base-iri
      (setq base-iri (www-uri-encode-feature-name base)))
    (if context
	(intern (format "%s@%s" base-iri context))
      base-iri)))

(defun chise-rdf-iri-encode-ccs-spec (base granularity context)
  (format "%s:%s"
	  granularity
	  (chise-rdf-iri-make-ccs@domain base context)))

(defun chise-rdf-iri-encode-ccs-name (feature-name)
  (apply #'chise-rdf-iri-encode-ccs-spec (chise-split-ccs-name feature-name)))

(defun chise-rdf-iri-decode-ccs-name (iri-feature)
  (let (iri-base base base-cobj iri-granularity ccs-prefix
		 iri-domain domain)
    (if (string-match "@" iri-feature)
	(setq iri-base (substring iri-feature 0 (match-beginning 0))
	      iri-domain (substring iri-feature (match-end 0)))
      (setq iri-base iri-feature))
    (if (string-match ":" iri-base)
	(setq iri-granularity (substring iri-base 0 (match-beginning 0))
	      iri-base (substring iri-base (match-end 0))))
    (when iri-granularity
      (setq base
	    (if (equal iri-base "ucs")
		'ucs
	      (or (and (setq base-cobj
			     (concord-decode-object
			      '=json-ld-item (intern iri-base) 'feature))
		       (concord-object-id base-cobj))
		  (est-uri-decode-feature-name iri-base))))
      (setq ccs-prefix (chise-encode-ccs-prefix (intern iri-granularity)))
      (setq domain
	    (if iri-domain
		(est-uri-decode-feature-name-body iri-domain)))
      (if domain
	  (intern (format "%s%s@%s" ccs-prefix base domain))
	(intern (format "%s%s" ccs-prefix base))))))

(defun chise-rdf-iri-encode-feature-name (feature-name)
  (or (char-feature-property feature-name '=json-ld-item)
      (if (find-charset feature-name)
	  (chise-rdf-iri-encode-ccs-name feature-name)
	(www-uri-encode-feature-name feature-name))))

(defun chise-rdf-iri-decode-feature-name (iri-feature-name)
  (let ((feature-cobj (concord-decode-object
		       '=json-ld-item (intern iri-feature-name) 'feature)))
    (or (if feature-cobj
	    (concord-object-id feature-cobj))
	(chise-rdf-iri-decode-ccs-name iri-feature-name)
	(est-uri-decode-feature-name iri-feature-name))))

(defun chise-rdf-iri-encode-object (object)
  (if (characterp object)
      (if (encode-char object '=ucs)
	  (concat
	   "character:"
	   (mapconcat
	    (lambda (byte)
	      (format "%%%02X" byte))
	    (encode-coding-string (char-to-string object) 'utf-8-mcs-er)
	    ""))
	(let ((ccs-list est-coded-charset-priority-list)
	      ccs ret)
	  (while (and ccs-list
		      (setq ccs (pop ccs-list))
		      (not (setq ret (encode-char object ccs 'defined-only)))))
	  (cond (ret
		 (format "%s/%s"
			 (chise-rdf-iri-encode-feature-name ccs)
			 (chise-ccs-format-code-point ccs ret "0x"))
		 )
		((and (setq ccs (car (split-char object)))
		      (setq ret (encode-char object ccs)))
		 (format "%s/%s"
			 (chise-rdf-iri-encode-feature-name ccs)
			 (chise-ccs-format-code-point ccs ret "0x"))
		 )
		(t
		 (format "system-char-id:0x%X"
			 (encode-char object 'system-char-id))
		 ))))
    (format "concord:%s/%s"
	    (chise-rdf-iri-encode-feature-name (concord-object-genre object))
	    (chise-rdf-iri-encode-feature-name (concord-object-id object)))))

(defun chise-rdf-iri-decode-object (object-rep)
  (let (prefix body ccs-prefix genre id base ccs)
    (cond
     ((string-match ":" object-rep)
      (setq prefix (substring object-rep 0 (match-beginning 0))
	    body (substring object-rep (match-end 0)))
      (cond
       ((equal prefix "concord")
	(when (string-match "/" body)
	  (setq genre (chise-rdf-iri-decode-feature-name
		       (substring body 0 (match-beginning 0)))
		id (chise-rdf-iri-decode-feature-name
		    (substring body (match-end 0))))
	  (concord-decode-object '=id id genre)
	  )
	)
       ((setq ccs-prefix (chise-encode-ccs-prefix (intern prefix)))
	(when (string-match "/" body)
	  (setq base (chise-rdf-iri-decode-feature-name
		      (substring body 0 (match-beginning 0)))
		id (substring body (match-end 0)))
	  (when (find-charset (setq ccs (intern (format "%s%s" ccs-prefix base))))
	    (cond
	     ((string-match "^0x" id)
	      (setq id (string-to-number (substring id (match-end 0)) 16))
	      )
	     (t
	      (setq id (car (read-from-string id)))
	      ))
	    (decode-char ccs id)
	    ))
	))
      ))
    ))

(defun chise-feature-name-split-metadata (feature-name)
  (let (ret)
    (setq feature-name (symbol-name feature-name))
    (if (string-match ".\\(\\$_\\([0-9]+\\)\\)?\\*." feature-name)
	(list (intern (substring feature-name 0 (1+ (match-beginning 0))))
	      (if (setq ret (match-string 2 feature-name))
		  (car (read-from-string ret)))
	      (intern (substring feature-name (1- (match-end 0))))))))

(defun chise-feature-name-split-domain (feature-name)
  (setq feature-name (symbol-name feature-name))
  (if (string-match "@[^@/*]+\\(/[^@/*]+\\)*$" feature-name)
      (cons (intern (substring feature-name 0 (match-beginning 0)))
	    (intern (substring feature-name (1+ (match-beginning 0)))))))


(defun chise-split-ccs-name (ccs)
  (cond ((eq ccs '=ucs)
	 '(ucs abstract-character nil)
	 )
	((eq ccs '=big5)
	 '(big5 abstract-character nil)
	 )
	(t
	 (let ((ccs-name (symbol-name
			  (let ((ccs-obj (find-charset ccs)))
			    (if ccs-obj
				(charset-name ccs-obj)
			      ccs))))
	       ret)
	   (if (string-match "^\\(=[=+>]*\\)\\([^=>@*]+\\)@?" ccs-name)
	       (list (intern (match-string 2 ccs-name))
		     (chise-decode-ccs-prefix (match-string 1 ccs-name))
		     (if (string= (setq ret (substring ccs-name (match-end 0))) "")
			 nil
		       (car (read-from-string ret))))
	     (list ccs 'abstract-character nil)))
	 )))

(defun chise-split-id-feature-name (id-feature)
  (let ((chise-ccs-prefix-alist
	 (cons '("=" . rep)
	       chise-ccs-prefix-alist)))
    (chise-split-ccs-name id-feature)))

(defconst chise-ccs-prefix-alist
  '(("==>" . super-abstract-character)
    ("==>" . a2)
    ("=>"  . abstract-character)
    ("=>"  . character)
    ("=>"  . a)
    ("=+>" . unified-glyph)
    ("=+>" . o)
    ("="   . abstract-glyph)
    ("="   . glyph)
    ("="   . rep)
    ("=>>" . detailed-glyph)
    ("=>>" . g)
    ("=="  . abstract-glyph-form)
    ("=="  . glyph-form)
    ("=="  . g2)
    ("===" . glyph-image)
    ("===" . repi)
    ))

(defun chise-decode-ccs-prefix (ccs-prefix)
  (or (cdr (assoc ccs-prefix chise-ccs-prefix-alist))
      'character))

(defun chise-encode-ccs-prefix (ccs-granularity)
  (car (rassoc ccs-granularity chise-ccs-prefix-alist)))

(defun chise-ccs-format-code-point (ccs value &optional hex-prefix rep-ccs)
  (let ((presentation-type
	 (char-feature-property ccs 'value-presentation-type))
	(presentation-minimum-width
	 (char-feature-property ccs 'value-presentation-minimum-width))
	ret ccs-base)
    (unless rep-ccs
      (cond ((find-charset ccs)
	     (setq rep-ccs ccs)
	     )
	    ((and (setq rep-ccs (intern (format "=%s" ccs)))
		  (find-charset rep-ccs))
	     )
	    ((and (setq rep-ccs (intern (format "=>%s" ccs)))
		  (find-charset rep-ccs))
	     )
	    ((and (setq rep-ccs (intern (format "==>%s" ccs)))
		  (find-charset rep-ccs))
	     )
	    ((and (setq rep-ccs (intern (format "===%s" ccs)))
		  (find-charset rep-ccs))
	     )))
    (unless hex-prefix
      (setq hex-prefix "#x"))
    (unless (and presentation-type presentation-minimum-width)
      (setq ret (chise-split-ccs-name ccs))
      (setq ccs-base (car ret))
      (unless presentation-type
	(setq presentation-type
	      (char-feature-property ccs-base 'value-presentation-type)))
      (unless presentation-minimum-width
	(setq presentation-minimum-width
	      (char-feature-property ccs-base 'value-presentation-minimum-width))))
    (format
     (cond
      ((eq presentation-type 'decimal)
       (if presentation-minimum-width
	   (format "%%0%dd" presentation-minimum-width)
	 "%d")
       )
      ((eq presentation-type 'HEX)
       (if presentation-minimum-width
	   (format "%s%%0%dX" hex-prefix presentation-minimum-width)
	 (format "%s%%X" hex-prefix))
       )
      ((eq presentation-type 'hex)
       (if presentation-minimum-width
	   (format "%s%%0%dx" hex-prefix presentation-minimum-width)
	 (format "%s%%x" hex-prefix))
       )
      (t
       (if (>= (charset-dimension rep-ccs) 2)
	   (format "%s%%04X" hex-prefix)
	 (format "%s%%02X" hex-prefix)
	 )
       ))
     (if (= (charset-iso-graphic-plane rep-ccs) 1)
	 (logior value
		 (cond ((= (charset-dimension rep-ccs) 1)
			#x80)
		       ((= (charset-dimension rep-ccs) 2)
			#x8080)
		       ((= (charset-dimension rep-ccs) 3)
			#x808080)
		       (t 0)))
       value))))

      
(defun chise-json-encode-feature-name (feature-name)
  (or (char-feature-property feature-name '=json-ld-item)
      (if (find-charset feature-name)
	  (chise-turtle-uri-encode-ccs-name feature-name)
	(www-uri-encode-feature-name feature-name))))

(defun chise-json-make-value-cell (value)
  (cons 'body
	(if (atom value)
	    value
	  (mapvector (lambda (obj)
		       (if (and (consp obj)
				(consp (car obj))
				(and (symbolp (caar obj))))
			   (concord-object-spec-to-full-nested-form obj)
			 obj))
		     value))))

(defun concord-object-spec-to-full-nested-form (spec-alist)
  (setq spec-alist (copy-tree spec-alist))
  (let (nested-spec
        feat val
        target target-base target-domain target-index
        meta meta-base meta-domain
        ret ret2 ret3 ret4 ret5)
    (dolist (cell (sort spec-alist
                        (lambda (a b)
                          (char-attribute-name< (car a)(car b)))))
      (setq feat (car cell)
            val (cdr cell))
      (if (eq feat 'composition)
	  (setq nested-spec
		(cons (list (car cell)
			    (list nil (cons 'body (vector (cdr cell)))))
		      nested-spec))
	(if (setq ret (chise-feature-name-split-metadata feat))
	    (setq target (car ret)
		  target-index (nth 1 ret)
		  meta (nth 2 ret))
	  (setq target feat
		target-index nil
		meta nil))
	(if (setq ret (chise-feature-name-split-domain target))
	    (setq target-base (car ret)
		  target-domain (cdr ret))
	  (setq target-base target
		target-domain nil))
	(if (setq ret (chise-feature-name-split-domain meta))
	    (setq meta-base (car ret)
		  meta-domain (cdr ret))
	  (setq meta-base meta
		meta-domain nil))
	(if (setq ret (assq target-base nested-spec))
	    (cond
	     ((setq ret2 (assq target-domain (cdr ret)))
	      (cond
	       (target-index
		(setq ret3 (assq 'body (cdr ret2)))
		(setq ret3 (cdr ret3))
		(setq ret4 (aref ret3 (1- target-index)))
		(if (and (consp ret4)
			 (assq 'body ret4))
		    (if (setq ret5 (assq meta-base ret4))
			(setcdr ret5 (cons (cons meta-domain
						 (list (chise-json-make-value-cell
							val)))
					   (cdr ret5)))
		      (aset ret3 (1- target-index)
			    (cons (cons meta-base
					(list (cons meta-domain
						    (list
						     (chise-json-make-value-cell
						      val)))))
				  ret4)))
		  (aset ret3 (1- target-index)
			(list (cons 'body ret4)
			      (cons meta-base
				    (list (cons meta-domain
						(list
						 (chise-json-make-value-cell
						  val))))))))
		)
	       (meta-base
		(cond
		 ((setq ret5 (assq meta-base (cdr ret2)))
		  (setcdr ret5 (cons (cons meta-domain
					   (chise-json-make-value-cell val))
				     (cdr ret5)))
		  )
		 (t
		  (setcdr ret2 (cons (cons meta-base
					   (list (cons meta-domain
						       (list
							(chise-json-make-value-cell
							 val)))))
				     (cdr ret2)))
		  ))
		))
	      )
	     (t
	      (setcdr ret (cons (cons target-domain
				      (list (chise-json-make-value-cell val)))
				(cdr ret)))
	      ))
	  (setq nested-spec
		(cons (cons target-base
			    (list (cons target-domain
					(list (chise-json-make-value-cell val)))))
		      nested-spec))
	  )))
    nested-spec))

(defun concord-nested-feature-domain-spec-simplify (dom-spec)
  (let (spec cell)
    (cond
     ((null (car dom-spec))
      (setq spec (cdr dom-spec))
      (if (null (cdr spec))
	  (if (and (setq cell (car spec))
		   (eq (car cell) 'body))
	      (cond
	       ((vectorp (cdr cell))
		(if (= (length (cdr cell)) 1)
		    (concord-full-nested-feature-simplify (aref (cdr cell) 0))
		  (mapvector #'concord-full-nested-feature-simplify (cdr cell)))
		)
	       ((atom (cdr cell))
		(cdr cell)
		)
	       ((atom (cadr cell))
		(mapvector #'identity (cdr cell))
		)
	       (t
		(concord-full-nested-feature-simplify spec)))
	    (concord-full-nested-feature-simplify spec))
	(concord-full-nested-feature-simplify spec))
      )
     ((eq (car dom-spec) 'body)
      dom-spec)
     (t
      (cons (cons 'context (format "domain:%s" (car dom-spec)))
	    (concord-full-nested-feature-simplify (cdr dom-spec)))
      ))
    ))

(defun concord-full-nested-feature-simplify (full-nested-feature)
  (if (atom full-nested-feature)
      full-nested-feature
    (mapcar (lambda (cell)
	      (if (eq (car cell) 'body)
		  (cond ((vectorp (cdr cell))
			 (cons (car cell)
			       (mapvector #'concord-full-nested-feature-simplify
					  (cdr cell)))
			 )
			((atom (cdr cell))
			 cell
			 )
			((atom (cadr cell))
			 (cons (car cell)
			       (mapvector #'identity cell))
			 )
			(t
			 (cons (car cell)
			       (mapvector #'concord-full-nested-feature-simplify
					  (cdr cell)))))
                (cons (car cell)
		      ;; (if (symbolp (car cell))
                      ;;     (chise-json-encode-feature-name (car cell))
                      ;;   (car cell))
		      (cond
		       ((atom (cdr cell))
			(cdr cell)
			)
		       ((null (cddr cell))
			(concord-nested-feature-domain-spec-simplify
			 (cadr cell))
			)
		       (t
			(mapvector #'concord-nested-feature-domain-spec-simplify
				   (cdr cell)))))))
	    full-nested-feature)))

(defun concord-object-spec-to-nested-form (spec-alist)
  (concord-full-nested-feature-simplify
   (concord-object-spec-to-full-nested-form
    spec-alist)))

(defun chise-json-separate-features (nested-spec)
  (let ((case-fold-search nil)
	ccs-spec
	subsumption-spec
	variant-spec
	glyph-example-spec
	other-rel-spec
	other-spec
	metadata-spec
	key)
    (dolist (cell nested-spec)
      (setq key (car cell))
      (if (find-charset key)
	  (setq ccs-spec (cons cell ccs-spec))
	(setq key (symbol-name key))
	(cond
	 ((string-match "^=" key)
	  (setq ccs-spec (cons cell ccs-spec))
	  )
	 ((string-match "^\\(<-\\|->\\)\\(denotational\\|subsumptive\\)" key)
	  (setq subsumption-spec (cons cell subsumption-spec))
	  )
	 ((string-match "^<-HNG" key)
	  (setq subsumption-spec (cons cell subsumption-spec))
	  )
	 ((string-match "^\\(<-\\|->\\)[A-Z][A-Z]" key)
	  (setq glyph-example-spec (cons cell glyph-example-spec))
	  )
	 ((string-match "^\\(<-\\|->\\)[A-Z][a-z]" key)
	  (setq other-rel-spec (cons cell other-rel-spec))
	  )
	 ((string-match "^\\(<-\\|->\\)" key)
	  (setq variant-spec (cons cell variant-spec))
	  )
	 ((string-match "^\\*" key)
	  (setq metadata-spec (cons cell metadata-spec))
	  )
	 (t
	  (setq other-spec (cons cell other-spec))
	  ))))
    (vector other-spec
	    ccs-spec
	    subsumption-spec
	    other-rel-spec
	    variant-spec
	    glyph-example-spec
	    metadata-spec)))

(defun chise-json-encode-general-feature-pair (feature-pair)
  (let ((feature-name (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	item context context-cell
	new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq item (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context-cell cell
			 context (cdr cell))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq item body))
      (if context
	  (setq new-body (cons context-cell new-body)))
      (setq dest (cons (if new-body
			   (if item
			       (cons (cons 'rdf:value item)
				     new-body)
			     new-body)
			 item)
		       dest))
      (setq i (1+ i)
	    new-body nil))
    (cons (chise-json-encode-feature-name feature-name)
	  (if (cdr dest)
	      (mapvector #'identity (nreverse dest))
	    (car dest)))))

(defun chise-json-encode-name-feature-pair (feature-pair)
  (let ((feature-name (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	item context
	lang
	new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq item (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context (cdr cell))
		   (setq lang
			 (if (string-match "^domain:" context)
			     (substring context (match-end 0))
			   context))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq item body))
      (setq dest (cons (cons (or lang "@none") item)
		       dest))
      (setq i (1+ i)
	    new-body nil))
    (cons (chise-json-encode-feature-name feature-name)
	  (if (or (cdr dest)
		  (not (string= (caar dest) "@none")))
	      dest
	    (cdar dest)))))

(defun chise-json-encode-ccs-feature-pair (feature-pair)
  (let ((ccs (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	code-point code-point-rep context context-cell
	ret base granularity
	bare-ccs@domain ccs@domain new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  ;; (setq code-point (cdr (assq 'body body))
	  ;;       context (cdr (assq 'context body)))
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq code-point (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context-cell cell
			 context (cdr cell))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq code-point body))
      (if (eq ccs '=ucs)
	  (if context
	      (setq base 'ucs
		    granularity 'abstract-glyph)
	    (setq base 'ucs
		  granularity 'abstract-character))
	(setq ret (chise-split-ccs-name ccs)
	      base (pop ret)
	      granularity (pop ret)))
      (setq code-point-rep
	    (chise-ccs-format-code-point
	     base code-point "0x"))
      (setq bare-ccs@domain
	    (if context
		(intern (format "%s@%s"
				base (substring context 7)))
	      base))
      (setq ccs@domain (intern (format "%s:%s" granularity bare-ccs@domain)))
      (if context
	  (setq new-body (cons context-cell new-body)))
      (setq dest
	    (cons
	     (cons ccs@domain
		   (list*
		    (cons '@id (format "%s/%s"
				       ccs@domain
				       code-point-rep))
		    (cons (intern (format "%s-of" granularity))
			  (list (cons '@id (format "bare-ccs:%s/%s"
						   base code-point-rep))
				(cons 'CCS (format "ccs:%s" base))
				(cons 'code-point code-point)))
		    new-body))
	     dest))
      (setq i (1+ i)
	    new-body nil))
    (nreverse dest)))

(defun chise-json-encode-id-feature-pair (feature-pair genre)
  (let ((id-feature (car feature-pair))
	(value (cdr feature-pair))
	(i 0)
	value value-rep context context-cell
	ret base granularity
	bare-id-feature@domain id-feature@domain new-body
	body len dest)
    (if (vectorp value)
	(setq len (length value))
      (setq value (vector value)
	    len 1))
    (while (< i len)
      (setq body (aref value i))
      (if (consp body)
	  (dolist (cell body)
	    (cond ((eq (car cell) 'body)
		   (setq value (cdr cell))
		   )
		  ((eq (car cell) 'context)
		   (setq context-cell cell
			 context (cdr cell))
		   )
		  (t
		   (setq new-body (cons cell new-body))
		   )))
	(setq value body))
      (setq ret (chise-split-id-feature-name id-feature)
	    base (pop ret)
	    granularity (pop ret))
      (setq value-rep (format "%s" value))
      (setq bare-id-feature@domain
	    (if context
		(intern (format "%s@%s"
				base (substring context 7)))
	      base))
      (setq id-feature@domain
	    (intern (format "%s:%s" granularity bare-id-feature@domain)))
      (if context
	  (setq new-body (cons context-cell new-body)))
      (setq dest
	    (cons
	     (cons id-feature@domain
		   (list*
		    (cons '@id (format "concord-idx:%s/%s/%s"
				       genre
				       id-feature@domain
				       value-rep))
		    (cons 'identifier
			  (list '(@type . "PropertyValue")
				(cons 'propertyID
				      (format "rep:%s" base))
				(cons 'value value)))
		    new-body))
	     dest))
      (setq i (1+ i)
	    new-body nil))
    (nreverse dest)))

(defun chise-json-encode-ccs-spec (ccs-spec)
  (cons 'unify
	(mapcan #'chise-json-encode-ccs-feature-pair ccs-spec)))

(defun chise-json-encode-id-spec (id-spec genre)
  (if (eq genre 'character)
      (chise-json-encode-ccs-spec id-spec)
    (cons 'unify
	  (mapcan (lambda (cell)
		    (chise-json-encode-id-feature-pair cell genre))
		  id-spec))))

(defun chise-json-encode-variant-body (val place-id json-feature-name)
  (let (body context source new-body)
    (if (atom val)
	(setq body val)
      (dolist (cell val)
	(cond
	 ((eq (car cell) 'body)
	  (setq body (cdr cell))
	  )
	 ((eq (car cell) 'context)
	  (setq context (cdr cell))
	  )
	 ((eq (car cell) 'sources)
	  (setq source (cdr cell))
	  )
	 (t
	  (setq new-body (cons cell new-body))
	  ))))
    (if source
	(setq new-body
	      (cons (cons 'source
			  (if (vectorp source)
			      (mapvector (lambda (item)
					   (format "domain:%s" item))
					 source)
			    (format "domain:%s" source)))
		    new-body)))
    (if context
	(setq new-body
	      (cons (cons 'context context)
		    new-body)))
    (list* (cons '@id place-id)
	   (list 'ideolink
		 (cons '@id (format "%s/link" place-id))
		 (cons json-feature-name
		       (if (vectorp body)
			   (mapvector #'chise-rdf-iri-encode-object body)
			 (chise-rdf-iri-encode-object body))))
	   new-body)))

(defun chise-json-encode-variant-feature-pair (feature-pair char-id)
  (let ((feature-name (car feature-pair))
	(val (cdr feature-pair))
	body context context-name source new-body
	json-feature-name i
	new-body-content
	node-id)
    (if (atom val)
	(setq body val)
      (dolist (cell val)
	(cond
	 ((eq (car cell) 'body)
	  (setq body (cdr cell))
	  )
	 ((eq (car cell) 'context)
	  (setq context (cdr cell))
	  )
	 ((eq (car cell) 'sources)
	  (setq source (cdr cell))
	  )
	 (t
	  (setq new-body (cons cell new-body))
	  ))))
    (setq json-feature-name
	  (chise-json-encode-feature-name feature-name))
    (if source
	(setq new-body
	      (cons (cons 'source
			  (if (vectorp source)
			      (mapvector (lambda (item)
					   (format "domain:%s" item))
					 source)
			    (format "domain:%s" source)))
		    new-body)))
    (if context
	(setq new-body
	      (cons (cons 'context context)
		    new-body)))
    (setq node-id
	  (format "%s/%s%s"
		  char-id json-feature-name
		  (if context
		      (progn
			(if (symbolp context)
			    (setq context-name (symbol-name context))
			  (setq context-name context))
			(if (string-match "^domain:" context-name)
			    (setq context-name
				  (substring context-name (match-end 0))))
			(format "@%s" context-name))
		    "")))
    (setq new-body-content
	  (cond
	   ((vectorp body)
	    (setq i 0)
	    (mapvector (lambda (item)
			 (setq i (1+ i))
			 (chise-json-encode-variant-body
			  item
			  (format "%s$_%d" node-id i)
			  json-feature-name))
		       body)
	    )
	   (t
	    (chise-json-encode-variant-body
	     body
	     (format "%s/%s" char-id json-feature-name)
	     json-feature-name)
	    )))
    (cons json-feature-name
	  (if new-body
	      (list* (cons '@id node-id)
		     (cons 'body new-body-content)
		     new-body)
	    new-body-content))))

(defun chise-json-encode-variant-spec (variant-spec char-id
						    &optional spec-category)
  (cons (or spec-category 'variant)
	(mapcar (lambda (feature-pair)
		  (chise-json-encode-variant-feature-pair feature-pair char-id))
		variant-spec)))

(defun concord-object-spec-to-json-ld-spec (spec-alist object)
  (let ((obj-rep (chise-rdf-iri-encode-object object))
	(ret (chise-json-separate-features spec-alist))
	(genre (if (characterp object)
		   'character
		 (concord-object-genre object)))
	general-spec id-spec
	granularity-spec interscript-spec
	variant-spec glyph-example-spec
	metadata-spec
	dest general-dest)
    (setq general-spec (aref ret 0)
	  id-spec      (aref ret 1)
	  granularity-spec   (aref ret 2)
	  interscript-spec   (aref ret 3)
	  variant-spec       (aref ret 4)
	  glyph-example-spec (aref ret 5)
	  metadata-spec      (aref ret 6))
    (dolist (cell metadata-spec)
      (setq dest
	    (cons (cons (chise-json-encode-feature-name (car cell))
			(cdr cell))
		  dest)))
    (setq dest (nreverse dest))
    (if glyph-example-spec
	(setq dest
	      (cons (chise-json-encode-variant-spec glyph-example-spec
						    obj-rep 'glyph-example)
		    dest)))
    (if variant-spec
	(setq dest
	      (cons (chise-json-encode-variant-spec variant-spec
						    obj-rep 'variant)
		    dest)))
    (if interscript-spec
	(setq dest
	      (cons (chise-json-encode-variant-spec interscript-spec
						    obj-rep 'interscript)
		    dest)))
    (if granularity-spec
	(setq dest
	      (cons (chise-json-encode-variant-spec granularity-spec
						   obj-rep 'intergranularity)
		    dest)))
    (if id-spec
	(setq dest
	      (cons (chise-json-encode-id-spec id-spec genre)
		    dest)))
    (dolist (cell general-spec)
      (setq general-dest
	    (cons (if (eq (car cell) 'name)
		      (chise-json-encode-name-feature-pair cell)
		    (chise-json-encode-general-feature-pair cell))
		  general-dest)))
    (dolist (cell general-dest)
      (setq dest (cons cell dest)))
    (list*
     (cons '@context "http://rdf.chise.org/contexts/chise.jsonld")
     (cons '@id obj-rep)
     dest)))

(defun concord-object-get-json-ld-spec (object &optional spec)
  (concord-object-spec-to-json-ld-spec
   (concord-object-spec-to-nested-form
    (or spec
	(if (characterp object)
	    (char-attribute-alist object)
	  (concord-object-spec object))))
   object))

(defun chise-json-api-character-decode (args)
  (let (ccs cpos char format-type ret)
    (when (and (setq ccs (assoc "ccs" args))
	       (setq ccs (cdr ccs))
	       (setq cpos (assoc "code-point" args))
	       (setq cpos (cdr cpos))
	       (setq ccs (chise-rdf-iri-decode-feature-name ccs))
	       (find-charset ccs))
      (cond
       ((string-match "^0x" cpos)
	(setq cpos (string-to-number (substring cpos (match-end 0)) 16))
	)
       (t
	(setq cpos (car (read-from-string cpos)))
	))
      (if (setq format-type (assoc "format" args))
	  (setq format-type (cdr format-type)))
      (when (and (integerp cpos)
		 (setq char (decode-char ccs cpos)))
	(cond
	 ((and format-type
	       (or (equal (downcase format-type) "est")
		   (equal (downcase format-type) "string")))
	  (encode-coding-string
	   (json-encode
	    (www-uri-encode-object char))
	   'utf-8-mcs-er)
	  )
	 ((equal format-type "json")
	  (chise-json-encode-object char)
	  )
	 (t
	  (setq ret (chise-json-encode-object* char))
	  (cond
	   ((stringp ret)
	    (cons
	     (encode-coding-string
	      (json-encode
	       (list '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     '(@type . genre:character)
		     (cons '@id (format "character:%s" ret))))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    )
	   (t
	    (cons
	     (encode-coding-string
	      (json-encode
	       (cons '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     ret))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    ))))))))

(defun chise-json-api-character-encode (args)
  (let (ccs char cpos)
    (when (and (setq char (assoc "character" args))
	       (setq char (cdr char))
	       (setq char (or (chise-rdf-iri-decode-object char)
			      (www-uri-decode-object 'character char)))
	       (setq ccs (assoc "ccs" args))
	       (setq ccs (cdr ccs))
	       (setq ccs (chise-rdf-iri-decode-feature-name ccs))
	       (find-charset ccs))
      (if (setq cpos (encode-char char ccs))
	  (format "%d" cpos)))))

(defun chise-json-api-character-get (args)
  (let (char feat val)
    (when (and (setq char (assoc "character" args))
	       (setq char (cdr char))
	       (setq char (or (chise-rdf-iri-decode-object char)
			      (www-uri-decode-object 'character char)))
	       (setq feat (assoc "feature" args))
	       (setq feat (cdr feat))
	       (setq feat (chise-rdf-iri-decode-feature-name feat)))
      (if (setq val (char-feature char feat))
	  (encode-coding-string (json-encode val) 'utf-8-mcs-er)))))

(defun chise-json-api-character-get-spec (args)
  (let ((media-type "application/json")
	char ret feature-format)
    (when (and (setq char (assoc "character" args))
	       (setq char (cdr char))
	       (setq char (or (chise-rdf-iri-decode-object char)
			      (www-uri-decode-object 'character char))))
      (when (setq ret (del-alist '*instance@ruimoku/bibliography/title
				 (del-alist '*instance@morpheme-entry/zh-classical
					    (del-alist 'ideographic-products
						       (char-attribute-alist char)))))
	(if (setq feature-format (assoc "feature-format" args))
	    (setq feature-format (cdr feature-format)))
	(cons (encode-coding-string
	       (json-encode
		(cond
		 ((equal feature-format "native")
		  (sort ret
			(lambda (a b)
			  (string< (car a)(car b))))
		  )
		 ((equal feature-format "json")
		  (mapcar (lambda (pair)
			    (cons (www-uri-encode-feature-name (car pair))
				  (cdr pair)))
			  (sort ret
				(lambda (a b)
				  (string< (car a)(car b)))))
		  )
		 (t ; (equal feature-format "json-ld")
		  (setq media-type "application/ld+json")
		  (concord-object-get-json-ld-spec char ret)
		  )))
	       'utf-8-mcs-er)
	      media-type)))))

(defun chise-json-api-character-ids-match (args)
  (let ((media-type "application/json")
	structure ret)
    (when (and (setq structure (assoc "ids" args))
	       (setq structure (cdr structure))
	       (setq structure (ids-parse-string
				(decode-uri-string structure 'utf-8-mcs-er)))
	       (setq structure (cdr (assq 'ideographic-structure structure)))
	       (setq ret (ideographic-structure-find-chars structure)))
      (cons (encode-coding-string (json-encode ret) 'utf-8-mcs-er)
	    media-type))))
      
(defun chise-batch-character-api ()
  (setq debug-on-error t)
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((method (pop command-line-args-left))
	     (target (pop command-line-args-left))
	     args
	     ret)
	(cond
	 ((stringp target)
	  (setq args
		(mapcar (lambda (cell)
			  (if (string-match "=" cell)
			      (cons
			       (substring cell 0 (match-beginning 0))
			       (substring cell (match-end 0)))
			    cell))
			(split-string target "&")))
	  (cond
	   ((equal method "decode")
	    (setq ret (chise-json-api-character-decode args))
	    )
	   ((equal method "encode")
	    (setq ret (chise-json-api-character-encode args))
	    )
	   ((equal method "get")
	    (setq ret (chise-json-api-character-get args))
	    )
	   ((equal method "get-spec")
	    (setq ret (chise-json-api-character-get-spec args))
	    )
	   ((equal method "ids-match")
	    (setq ret (chise-json-api-character-ids-match args))
	    ))
	  (cond
	   ((consp ret)
	    (princ (format "Content-Type: %s

%s"
			   (cdr ret)
			   (car ret)))
	    )
	   ((stringp ret)
	    (princ (format "Content-Type: application/json

%s"
			   ret))
	    )
	   (t
	    (princ (format "Content-Type: application/json

null
"))
	    ))
	  )))
    (error nil
	   (princ "Content-Type: text/plain; charset=UTF-8

")
	   (princ (format "{ \"error\": \"%S\" }" err))))
  (princ "\n"))

(defun chise-json-api-object-decode (args)
  (let (feature value genre obj format-type ret)
    (when (and (setq genre (or (assoc "genre" args)
			       (assoc "type" args)))
	       (setq genre (cdr genre))
	       (setq genre (chise-rdf-iri-decode-feature-name genre))
	       (setq feature (assoc "feature" args))
	       (setq feature (cdr feature))
	       (setq feature (chise-rdf-iri-decode-feature-name feature))
	       (setq value (assoc "value" args))
	       (setq value (cdr value)))
      (cond
       ((string-match "^0x" value)
	(setq value (string-to-number (substring value (match-end 0)) 16))
	)
       ;; ((setq ret (read-from-string value))
       ;;  (setq value (car ret))
       ;;  )
       (t
	(setq value (est-uri-decode-feature-name value))
	))
      (if (setq format-type (assoc "format" args))
	  (setq format-type (cdr format-type)))
      (when (setq obj (concord-decode-object feature value genre))
	(cond
	 ((equal format-type "EsT")
	  (encode-coding-string
	   (json-encode
	    (www-uri-encode-object obj))
	   'utf-8-mcs-er)
	  )
	 ((equal format-type "json")
	  (chise-json-encode-object obj)
	  )
	 (t
	  (setq ret (chise-json-encode-object* obj))
	  (cond
	   ((stringp ret)
	    (cons
	     (encode-coding-string
	      (json-encode
	       (list '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     '(@type . genre:character)
		     (cons '@id (format "character:%s" ret))))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    )
	   (t
	    (cons
	     (encode-coding-string
	      (json-encode
	       (cons '(@context . "http://rdf.chise.org/contexts/chise.jsonld")
		     ret))
	      'utf-8-mcs-er)
	     "application/ld+json")
	    ))))))))

(defun chise-json-api-object-get (args)
  (let (obj feat val)
    (when (and (setq obj (assoc "object" args))
	       (setq obj (cdr obj))
	       (setq obj (chise-rdf-iri-decode-object obj))
	       (setq feat (assoc "feature" args))
	       (setq feat (cdr feat))
	       (setq feat (chise-rdf-iri-decode-feature-name feat)))
      (if (setq val (concord-object-get obj feat))
	  (encode-coding-string (json-encode val) 'utf-8-mcs-er)))))

(defun chise-json-api-object-get-spec (args)
  (let ((media-type "application/json")
	obj ret feature-format)
    (when (and (setq obj (assoc "object" args))
	       (setq obj (cdr obj))
	       (setq obj (chise-rdf-iri-decode-object obj)))
      (when (setq ret (concord-object-spec obj))
	(if (setq feature-format (assoc "feature-format" args))
	    (setq feature-format (cdr feature-format)))
	(cons (encode-coding-string
	       (json-encode
		(cond
		 ((equal feature-format "native")
		  (sort ret
			(lambda (a b)
			  (string< (car a)(car b))))
		  )
		 ((equal feature-format "json")
		  (mapcar (lambda (pair)
			    (cons (www-uri-encode-feature-name (car pair))
				  (cdr pair)))
			  (sort ret
				(lambda (a b)
				  (string< (car a)(car b)))))
		  )
		 (t ; (equal feature-format "json-ld")
		  (setq media-type "application/ld+json")
		  (concord-object-get-json-ld-spec obj ret)
		  )))
	       'utf-8-mcs-er)
	      media-type)))))


(defun chise-json-get-radical-strokes (char)
  (let ((radical (get-char-attribute char 'ideographic-radical))
	(strokes (get-char-attribute char 'ideographic-strokes))
	spec dest rad-cell rad-name-cell str-cell domain)
    (if radical
	(progn
	  (setq rad-cell (cons 'kangxi-radical-number radical)
		rad-name-cell (cons 'kangxi-radical
				    (char-to-string (ideographic-radical radical))))
	  (if strokes
	      (list
	       (list rad-cell
		     rad-name-cell
		     (cons 'kangxi-strokes strokes)))
	    (setq spec (char-attribute-alist char))
	    (dolist (cell spec)
	      (when (string-match "^ideographic-strokes@\\([^*]+\\)$"
				  (symbol-name (car cell)))
		(setq domain (intern (match-string 1 (symbol-name (car cell)))))
		(setq dest
		      (cons (list rad-cell
				  rad-name-cell
				  (cons 'kangxi-strokes (cdr cell))
				  (cons 'domain domain))
			    dest))))
	    dest))
      (if strokes
	  (progn
	    (setq str-cell (cons 'kangxi-strokes strokes))
	    (setq spec (char-attribute-alist char))
	    (dolist (cell spec)
	      (when (string-match "^ideographic-radical@\\([^*]+\\)$"
				  (symbol-name (car cell)))
		(setq domain (intern (match-string 1 (symbol-name (car cell)))))
		(setq dest
		      (cons (list (cons 'kangxi-radical-number (cdr cell))
				  (cons 'kangxi-radical
					(char-to-string (ideographic-radical (cdr cell))))
				  str-cell
				  (cons 'domain domain))
			    dest))))
	    dest)
	(setq spec (char-attribute-alist char))
	(dolist (cell spec)
	  (when (string-match "^ideographic-radical@\\([^*]+\\)$"
			      (symbol-name (car cell)))
	    (setq domain (intern (match-string 1 (symbol-name (car cell)))))
	    (setq strokes (get-char-attribute
			   char
			   (intern (format "ideographic-strokes@%s" domain))))
	    (setq dest
		  (cons (list* (cons 'kangxi-radical-number (cdr cell))
			       (cons 'kangxi-radical
				     (char-to-string (ideographic-radical (cdr cell))))
			       (if strokes
				   (list (cons 'kangxi-strokes strokes)
					 (cons 'domain domain))
				 (cons 'domain domain)))
			dest))))
	dest))))


(defun chise-batch-object-api ()
  (setq debug-on-error t)
  (setq terminal-coding-system 'binary)
  (condition-case err
      (let* ((method (pop command-line-args-left))
	     (target (pop command-line-args-left))
	     args
	     ret)
	(cond
	 ((stringp target)
	  (setq args
		(mapcar (lambda (cell)
			  (if (string-match "=" cell)
			      (cons
			       (substring cell 0 (match-beginning 0))
			       (substring cell (match-end 0)))
			    cell))
			(split-string target "&")))
	  (cond
	   ((equal method "decode")
	    (setq ret (chise-json-api-object-decode args))
	    )
           ;; ((equal method "encode")
           ;;  (setq ret (chise-json-api-object-encode args))
           ;;  )
	   ((equal method "get")
	    (setq ret (chise-json-api-object-get args))
	    )
	   ((equal method "get-spec")
	    (setq ret (chise-json-api-object-get-spec args))
	    ))
	  (cond
	   ((consp ret)
	    (princ (format "Content-Type: %s

%s"
			   (cdr ret)
			   (car ret)))
	    )
	   ((stringp ret)
	    (princ (format "Content-Type: application/json

%s"
			   ret))
	    )
	   (t
	    (princ (format "Content-Type: application/json

null
"))
	    ))
	  )))
    (error nil
	   (princ "Content-Type: text/plain; charset=UTF-8

")
	   (princ (format "{ \"error\": \"%S\" }" err))))
  (princ "\n"))
