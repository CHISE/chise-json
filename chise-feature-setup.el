;; -*- coding: utf-8-mcs-er -*-
(setq file-name-coding-system 'utf-8-mcs-er)

(concord-object-put (concord-make-object
		     'feature 'iwds-1) 'name "IWDS 1 (List of UCV)")
(put-char-feature-property 'iwds-1 'value-presentation-type 'decimal)
(put-char-feature-property 'iwds-1 'value-presentation-minimum-width 4)

(concord-object-put (concord-make-object
		     'feature 'shinjigen) 'name "ShinJigen")
(put-char-feature-property 'shinjigen 'value-presentation-type 'decimal)
(put-char-feature-property 'shinjigen 'value-presentation-minimum-width 4)

(concord-object-put (concord-make-object
		     'feature 'shinjigen/+p) 'name "ShinJigen (dddd′)")
(put-char-feature-property 'shinjigen/+p 'value-presentation-type 'decimal)
(put-char-feature-property 'shinjigen/+p 'value-presentation-minimum-width 4)
(concord-object-put (concord-make-object 'feature 'shinjigen/+p)
		    '=json-ld-item 'shinjigen_p)

(concord-object-put (concord-make-object
		     'feature 'daikanwa/ho) 'name "Daikanwa hokan")
(concord-object-put (concord-make-object
		     'feature 'daikanwa/ho) 'name@ja "大漢和辭典 補")
(put-char-feature-property 'daikanwa/ho 'value-presentation-type 'decimal)
(put-char-feature-property 'daikanwa/ho 'value-presentation-minimum-width 4)
(concord-object-put (concord-make-object 'feature 'daikanwa/ho)
		    '=json-ld-item 'daikanwa_ho)

(concord-object-put (concord-make-object
		     'feature 'daikanwa) 'name@ja "大漢和辭典")
(put-char-feature-property 'daikanwa 'value-presentation-type 'decimal)
(put-char-feature-property 'daikanwa 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'daikanwa/+p) 'name@ja "大漢和辭典 (ddddd′)")
(put-char-feature-property 'daikanwa/+p 'value-presentation-type 'decimal)
(put-char-feature-property 'daikanwa/+p 'value-presentation-minimum-width 5)
(concord-object-put (concord-make-object 'feature 'daikanwa/+p)
		    '=json-ld-item 'daikanwa_p)

(concord-object-put (concord-make-object
		     'feature 'daikanwa/+2p) 'name@ja "大漢和辭典 (ddddd″)")
(put-char-feature-property 'daikanwa/+2p 'value-presentation-type 'decimal)
(put-char-feature-property 'daikanwa/+2p 'value-presentation-minimum-width 5)
(concord-object-put (concord-make-object 'feature 'daikanwa/+2p)
		    '=json-ld-item 'daikanwa_2p)

(concord-object-put (concord-make-object
		     'feature 'gt) 'name "GT")
(put-char-feature-property 'gt 'value-presentation-type 'decimal)
(put-char-feature-property 'gt 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'gt-k) 'name "GT-k")
(put-char-feature-property 'gt-k 'value-presentation-type 'decimal)
(put-char-feature-property 'gt-k 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'adobe-japan1-0) 'name "Adobe-Japan1-0")
(put-char-feature-property 'adobe-japan1-0 'value-presentation-type 'decimal)
(put-char-feature-property 'adobe-japan1-0 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'adobe-japan1-1) 'name "Adobe-Japan1-1")
(put-char-feature-property 'adobe-japan1-1 'value-presentation-type 'decimal)
(put-char-feature-property 'adobe-japan1-1 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'adobe-japan1-2) 'name "Adobe-Japan1-2")
(put-char-feature-property 'adobe-japan1-2 'value-presentation-type 'decimal)
(put-char-feature-property 'adobe-japan1-2 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'adobe-japan1-3) 'name "Adobe-Japan1-3")
(put-char-feature-property 'adobe-japan1-3 'value-presentation-type 'decimal)
(put-char-feature-property 'adobe-japan1-3 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'adobe-japan1-4) 'name "Adobe-Japan1-4")
(put-char-feature-property 'adobe-japan1-4 'value-presentation-type 'decimal)
(put-char-feature-property 'adobe-japan1-4 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'adobe-japan1-5) 'name "Adobe-Japan1-5")
(put-char-feature-property 'adobe-japan1-5 'value-presentation-type 'decimal)
(put-char-feature-property 'adobe-japan1-5 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'adobe-japan1-6) 'name "Adobe-Japan1-6")
(put-char-feature-property 'adobe-japan1-6 'value-presentation-type 'decimal)
(put-char-feature-property 'adobe-japan1-6 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'cbeta) 'name "CBETA PUA")
(put-char-feature-property 'cbeta 'value-presentation-type 'decimal)
(put-char-feature-property 'cbeta 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'zinbun-oracle) 'name "Oracle Bones Script in Zinbun")
(put-char-feature-property 'zinbun-oracle 'value-presentation-type 'decimal)
(put-char-feature-property 'zinbun-oracle 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'daijiten) 'name "Daijiten")
(put-char-feature-property 'daijiten 'value-presentation-type 'decimal)
(put-char-feature-property 'daijiten 'value-presentation-minimum-width 5)

(dolist (ccs '(hng-jou
	       hng-keg hng-dng hng-mam
	       hng-drt hng-kgk hng-myz hng-kda
	       hng-khi hng-khm hng-fhs hng-hok
	       hng-kyd hng-sok
	       hng-yhk hng-kak hng-kar hng-kae
	       hng-sys hng-tsu hng-tzj
	       hng-hos hng-kkh hng-nak hng-jhk
	       hng-hod hng-gok hng-ink hng-nto
	       hng-nkm hng-k24 hng-ini hng-nkk
	       hng-kcc hng-kcj hng-kbk hng-sik
	       hng-skk hng-kyu hng-ksk hng-wan
	       hng-okd hng-wad hng-kmi
	       hng-zkd hng-doh hng-jyu hng-tzs
	       hng-sai hng-kad hng-kss
	       hng-kyo hng-ykk hng-saa
	       hng-sab hng-wks hng-wke hng-smk
	       hng-sgs hng-sts))
  (concord-object-put (concord-make-object
		       'feature ccs)
		      'name@ja
		      (decode-coding-string (charset-description
					     (intern (format "===%s" ccs)))
					    'utf-8-mcs-er))
  (put-char-feature-property ccs 'value-presentation-type 'decimal)
  (put-char-feature-property ccs 'value-presentation-minimum-width 5))

(concord-object-put (concord-make-object
		     'feature 'shuowen-jiguge) 'name@ja "&MJ015106;古閣本説文&AJ1-01394;字")
(put-char-feature-property 'shuowen-jiguge 'value-presentation-type 'decimal)
(put-char-feature-property 'shuowen-jiguge 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'shuowen-jiguge4) 'name@ja "淮&AJ1-03270;&AJ1-02427;局4次&AJ1-03895;本説文&AJ1-01394;字")
(put-char-feature-property 'shuowen-jiguge4 'value-presentation-type 'decimal)
(put-char-feature-property 'shuowen-jiguge4 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'shuowen-jiguge5) 'name@ja "&MJ015106;古閣&MJ025932;行本説文&AJ1-01394;字")
(put-char-feature-property 'shuowen-jiguge5 'value-presentation-type 'decimal)
(put-char-feature-property 'shuowen-jiguge5 'value-presentation-minimum-width 5)

(concord-object-put (concord-make-object
		     'feature 'hanyo-denshi/ks) 'name "Han'you-Denshi/KS (KoSeki)")
(put-char-feature-property 'hanyo-denshi/ks 'value-presentation-type 'decimal)
(put-char-feature-property 'hanyo-denshi/ks 'value-presentation-minimum-width 6)

(concord-object-put (concord-make-object
		     'feature 'koseki) 'name "KoSeki-touitsu-moji")
(put-char-feature-property 'koseki 'value-presentation-type 'decimal)
(put-char-feature-property 'koseki 'value-presentation-minimum-width 6)

(concord-object-put (concord-make-object
		     'feature 'mj) 'name "Moji-Jouhou-Kiban")
(put-char-feature-property 'mj 'value-presentation-type 'decimal)
(put-char-feature-property 'mj 'value-presentation-minimum-width 6)

(concord-object-put (concord-make-object
		     'feature 'hanyo-denshi/tk) 'name "Han'you-Denshi/TK (TouKi)")
(put-char-feature-property 'hanyo-denshi/tk 'value-presentation-type 'decimal)
(put-char-feature-property 'hanyo-denshi/tk 'value-presentation-minimum-width 8)

(concord-object-put (concord-make-object 'feature 'hanyo-denshi/ja)
		    '=json-ld-item 'hanyo-denshi_ja)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/jb)
		    '=json-ld-item 'hanyo-denshi_jb)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/jc)
		    '=json-ld-item 'hanyo-denshi_jc)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/jd)
		    '=json-ld-item 'hanyo-denshi_jd)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/ft)
		    '=json-ld-item 'hanyo-denshi_ft)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/ia)
		    '=json-ld-item 'hanyo-denshi_ia)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/ib)
		    '=json-ld-item 'hanyo-denshi_ib)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/hg)
		    '=json-ld-item 'hanyo-denshi_hg)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/ip)
		    '=json-ld-item 'hanyo-denshi_ip)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/jt)
		    '=json-ld-item 'hanyo-denshi_jt)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/ks)
		    '=json-ld-item 'hanyo-denshi_ks)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/tk)
		    '=json-ld-item 'hanyo-denshi_tk)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/ks/mf)
		    '=json-ld-item 'hanyo-denshi_ks_mf)
(concord-object-put (concord-make-object 'feature 'hanyo-denshi/tk/mf-01)
		    '=json-ld-item 'hanyo-denshi_tk_mf-01)

(concord-object-put (concord-make-object 'feature '->subsumptive)
		    '=json-ld-item 'subsume)
(concord-object-put (concord-make-object 'feature '<-subsumptive)
		    '=json-ld-item 'subsumed-in)
(concord-object-put (concord-make-object 'feature '->denotational)
		    '=json-ld-item 'denotation)
(concord-object-put (concord-make-object 'feature '<-denotational)
		    '=json-ld-item 'connotation)

(concord-object-put (concord-make-object 'feature '->formed)
		    '=json-ld-item 'formed)
(concord-object-put (concord-make-object 'feature '<-formed)
		    '=json-ld-item 'form-of)
(concord-object-put (concord-make-object 'feature '->same)
		    '=json-ld-item 'ideo:same)
(concord-object-put (concord-make-object 'feature '<-same)
		    '=json-ld-item 'ideo:same-as)
(concord-object-put (concord-make-object 'feature '->simplified)
		    '=json-ld-item 'ideo:simplified-form)
(concord-object-put (concord-make-object 'feature '<-simplified)
		    '=json-ld-item 'ideo:simplified-form-of)
(concord-object-put (concord-make-object 'feature '->vulgar)
		    '=json-ld-item 'ideo:vulgar-form)
(concord-object-put (concord-make-object 'feature '<-vulgar)
		    '=json-ld-item 'ideo:vulgar-form-of)
(concord-object-put (concord-make-object 'feature '->wrong)
		    '=json-ld-item 'ideo:wrong-form)
(concord-object-put (concord-make-object 'feature '<-wrong)
		    '=json-ld-item 'ideo:wrong-form-of)
(concord-object-put (concord-make-object 'feature '->original)
		    '=json-ld-item 'ideo:original-form)
(concord-object-put (concord-make-object 'feature '<-original)
		    '=json-ld-item 'ideo:original-form-of)
(concord-object-put (concord-make-object 'feature '->ancient)
		    '=json-ld-item 'ideo:ancient-form)
(concord-object-put (concord-make-object 'feature '<-ancient)
		    '=json-ld-item 'ideo:ancient-form-of)
(concord-object-put (concord-make-object 'feature '->interchangeable)
		    '=json-ld-item 'ideo:interchangeable-form)
(concord-object-put (concord-make-object 'feature '<-interchangeable)
		    '=json-ld-item 'ideo:interchangeable)
(concord-object-put (concord-make-object 'feature '->mistakable)
		    '=json-ld-item 'ideo:mistakable-character)
(concord-object-put (concord-make-object 'feature '<-mistakable)
		    '=json-ld-item 'ideo:mistakable-character-of)
(concord-object-put (concord-make-object 'feature '->HNG)
		    '=json-ld-item 'hng:rep-glyph)
(concord-object-put (concord-make-object 'feature '<-HNG)
		    '=json-ld-item 'hng:rep-char)
(concord-object-put (concord-make-object 'feature '->Oracle-Bones)
		    '=json-ld-item 'ideo:Oracle-Bones)
(concord-object-put (concord-make-object 'feature '<-Oracle-Bones)
		    '=json-ld-item 'ideo:Oracle-Bones-of)
(concord-object-put (concord-make-object 'feature '->Small-Seal)
		    '=json-ld-item 'ideo:Small-Seal)
(concord-object-put (concord-make-object 'feature '<-Small-Seal)
		    '=json-ld-item 'ideo:Small-Seal-of)

(concord-object-put (concord-make-object 'feature 'name)
		    '=json-ld-item 'name)

(concord-object-put (concord-make-object 'feature 'sound)
		    '=json-ld-item 'chise:sound)

(concord-object-put (concord-make-object 'feature 'abstract-glyph)
		    '=json-ld-item 'chise:abstract-glyph)

(concord-object-put (concord-make-object 'feature 'composition)
		    '=json-ld-item 'chise:composition)

(concord-object-put (concord-make-object 'feature 'ideographic-radical)
		    '=json-ld-item 'ideo:radical)

(concord-object-put (concord-make-object 'feature 'ideographic-strokes)
		    '=json-ld-item 'ideo:strokes)

(concord-object-put (concord-make-object 'feature 'total-strokes)
		    '=json-ld-item 'ideo:total-strokes)

(concord-object-put (concord-make-object 'feature 'ideographic-structure)
		    '=json-ld-item 'ideo:structure)

(concord-object-put (concord-make-object 'feature 'ideographic-products)
		    '=json-ld-item 'ideo:products)

(concord-object-put (concord-make-object 'feature 'daijiten-pages)
		    '=json-ld-item 'ideo:daijiten-pages)

(concord-object-put (concord-make-object 'feature 'hanyu-dazidian)
		    '=json-ld-item 'ideo:hanyu-dazidian)

(concord-object-put (concord-make-object 'feature '*note)
		    '=json-ld-item 'rdfs:comment)

(concord-object-put (concord-make-object 'feature '*references)
		    '=json-ld-item 'reference)
(concord-object-put (concord-make-object 'feature '*instance)
		    '=json-ld-item 'instance)
(concord-object-put (concord-make-object 'feature 'source-file)
		    '=json-ld-item 'source-file)
